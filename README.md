## PyHK v3.0 ##

Python housekeeping software for laboratory cryogenic systems.

+ pykhd: data acquisition and storage
+ pyhkfridge: fridge control scripting
+ pyhkweb: web viewer for pyhkd and pyhkfridge data and control
+ pyhkcmd: basic terminal commands for pyhkd and pyhkfridge data and control

See http://docs.pyhk.net for setup, configuration, and usage instructions.

Repository Branch Guide:

+ `master`: main stable branch
+ `beta`: newer version under field testing, usually functional but possibly buggy
+ `develop`: under active development, possibly broken by partially-implemented changes
+ `legacy-py2`: older Python 2.7 version of PyHK, still active on some cryostats

