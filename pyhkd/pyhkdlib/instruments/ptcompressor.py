import os
import logging
import time
import serial
import math
import struct

from pyhkdlib.sensor import Sensor
from pyhkdlib.instruments.serial_instrument import SerialInstrument

STX = b'\x02'
ADDR = b'\x10'
CMD = b'\x80'
CR = b'\x0D'
DATA_WRITE = b'\x61'
DATA_READ = b'\x63'
ESC = b'\x07'
ESC_STX = b'\x30'
ESC_CR = b'\x31'
ESC_ESC = b'\x32'

class PTCompressor(SerialInstrument):

	chan_lookup = {
		b'\x45\x4C\x00':{'name':'Compressor Minutes', 'type':'state'},
		b'\x5F\x95\x00':{'name':'Status', 'type':'state'},
		b'\x65\xA4\x00':{'name':'Error', 'type':'state'},
		b'\x0D\x8F\x00':{'name':'H20 In', 'type':'temperature'},
		b'\x0D\x8F\x01':{'name':'H20 Out', 'type':'temperature'},
		b'\x0D\x8F\x02':{'name':'Helium', 'type':'temperature'},
		b'\x0D\x8F\x03':{'name':'Oil', 'type':'temperature'},
		b'\x7E\x90\x00':{'name':'Avg High', 'type':'pressure'},
		b'\xBB\x94\x00':{'name':'Avg Low', 'type':'pressure'},
		b'\x31\x9C\x00':{'name':'Avg Delta', 'type':'pressure'},
		b'\x66\xFA\x00':{'name':'High Derivative', 'type':'pressure'},
		b'\x63\x8B\x00':{'name':'Motor Current', 'type':'current'},
		}
	
	NUM_SENSORS = len(list(chan_lookup.keys())) + 1
	
	STATE_NO_CHANGE = 0
	STATE_TURN_ON = 1
	STATE_TURN_OFF = 2
	
	# ext_test_mode is a flag allowing this class to be used outside
	# of pyhkd for testing, and should not be used normally
	def __init__(self, port, name_prefix, baudrate = 9600, ext_test_mode = False, **kwargs):
		
		self.last_update_time = 0
		self.name_prefix = name_prefix
		self.ext_test_mode = ext_test_mode
		
		if ext_test_mode:
			
			# External testing mode, running outside of pyhkd
			channels = []
			self.NUM_SENSORS = 0
			
		else:
			
			# Add the name prefix
			for k in list(self.chan_lookup.keys()):
				self.chan_lookup[k]['name'] = name_prefix + ' ' + self.chan_lookup[k]['name']
			self._pt_state_name = name_prefix + ' ' + "On State"
			
			channels = list(self.chan_lookup.values())
			channels.append({'name': self._pt_state_name, 'type': Sensor.TYPE_TARGET_STATE})
					
		SerialInstrument.__init__(self, 
			port, 
			baudrate = baudrate, 
			pkt_end = CR,
			pkt_start = STX,
			channels = channels,
			return_bytes = True,
			**kwargs)

	def handle_packet(self, msg):
		
		#~ logging.debug("PT packet: " + ":".join("{:02x}".format(c) for c in msg))
		
		if (len(msg) == 4) and (msg[2] == '\x39') and (msg[3] == '\x39'):
			logging.info("PT state change acknowledged")
			return
		
		addr, val = self._deflate_packet(msg)
		
		if (val is None) or (addr is None) or (addr.startswith(b'\x66\x33')):
			return
		
		chan = self.chan_lookup.get(bytes(addr), None)
		
		if chan is None:
			logging.error("Recieved unidentified packet from PT compressor: " + ":".join("{:02x}".format(c) for c in msg) + " (addr " + ":".join("{:02x}".format(c) for c in addr) + ")")
			return
		
		if self.verbose_rx:
			logging.debug("PT Compressor RX: %s = %s" % (chan['name'], val))
				
		if chan['type'] == 'temperature':
			# 0.1 C to K
			val = val/10.0 + 273
		elif chan['type'] == 'pressure':
			# 0.1 PSI Absolute to Torr
			val = (val/10.0) * 51.71
			
		if not self.ext_test_mode:
			
			# Store the value
			sensor_index = self.lookup_id[chan['name']]
			sensor_type = self.lookup_typelist[chan['name']][0]
			self.get_sensor(sensor_index, sensor_type).value = val
		
	def update(self):
		
		if not self.ext_test_mode:
				
			targ = self.targets[Sensor.TYPE_TARGET_STATE][self._pt_state_name].value
							
			# Check if we need to turn the PT on or off
			if targ == self.STATE_TURN_ON:
				logging.info("Received request to turn the PT on")
				self._compressor_state(on=True)
				self.targets[Sensor.TYPE_TARGET_STATE][self._pt_state_name].value = self.STATE_NO_CHANGE
			elif targ == self.STATE_TURN_OFF:
				logging.info("Received request to turn the PT off")
				self._compressor_state(on=False)
				self.targets[Sensor.TYPE_TARGET_STATE][self._pt_state_name].value = self.STATE_NO_CHANGE
			else:
				pass

		# Check if certain amount of time has passed since something was read
		if ((time.time() - self.last_update_time) >= self.wait_time):
			
			self.last_update_time = time.time()
			
			if self.verbose_tx:
				logging.debug("Requesting PTC update")
			
			# Request updates
			for k in list(self.chan_lookup.keys()):
				m = self._make_packet(k)
				self.send_packet(m)
	
	def _make_packet(self, dhash, val=None):
		msg = ADDR + CMD
		if val is None:
			msgtype = DATA_READ
		else:
			msgtype = DATA_WRITE
		msg += msgtype
		payload = dhash
		if val is not None:
			payload += struct.pack('>i',val)
		table = {STX : ESC_STX, CR : ESC_CR, ESC : ESC_ESC}
		for i in range(len(payload)):
			if payload[i:i+1] in table:
				msg += ESC
				msg += table[payload[i:i+1]]
			else:
				msg += payload[i:i+1]
		cksum = self._checksum(ADDR+CMD+msgtype+payload)
		msg += cksum
		
		if self.verbose_tx:
			logging.debug("Built PT packet: " + ":".join("{:02x}".format(c) for c in msg))
		
		return msg

	def _checksum(self, msg):
		cksum = sum([x for x in msg])
		cksum0 = ((cksum & 0xF0) >> 4) + 0x30
		cksum1 = (cksum & 0x0F) + 0x30
		return bytes([cksum0,cksum1])

	def _deflate_packet(self, msg):
		
		if len(msg) < 6:
			hex_msg = ":".join("{:02x}".format(c) for c in msg)
			logging.error("PT compressor packet rx : too short (%s)" % (hex_msg,))
			return None, None

		msg = msg.replace(ESC+ESC_STX,STX)
		msg = msg.replace(ESC+ESC_CR,CR)
		msg = msg.replace(ESC+ESC_ESC,ESC)

		cksum = self._checksum(msg[0:-2])
		if cksum[0] != msg[-2] or cksum[1] != msg[-1]:
			logging.error("PT compressor packet rx error: bad checksum")
			return None, None
		
		addr = msg[3:6]
		data = struct.unpack('>I',msg[-6:-2])[0]
		return addr, data
		
	def _compressor_state(self, on):
		
		if on:
			logging.info("Turning PT compressor on")
			m = self._make_packet(b'\xD5\x01\00',1)
		else:
			logging.info("Turning PT compressor off")
			#m = self._make_packet(b'\xC5\x98\00',0) # Old compressors
			m = self._make_packet(b'\xC5\x98\00',1) # New compressors (sigh)
		
		self.send_packet(m)
		
		# Get state updates soon
		self.last_update_time = time.time() - (self.wait_time - 10)
		

