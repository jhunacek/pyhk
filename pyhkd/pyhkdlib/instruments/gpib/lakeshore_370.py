'''
Read and control a Lakeshore 370
'''

import time
import numpy as np
import logging
import os
import json
import traceback
import threading
import serial

from .gpib_instrument import AbstractSCPIInstrument, GPIBSCPIInstrument, SerialSCPIInstrument
from pyhkdlib.sensor import Sensor
from livecfg.livecfg import LiveCfg
from pyhkdremote.data_loader import pyhkd_get_config_dir
from calib.helpers import get_calib
		
# Control logic shared between the GPIB and Serial interfaces
class AbstractLakeshore370:
	
	NUM_SENSORS = 16
	BOX_TYPE = 'LS370'
	IDN_STR = 'MODEL37' # Allow 370 or 372
	
	# Excitation modes for RDGRNG
	MODE_VOLTAGE = 0
	MODE_CURRENT = 1
	
	RATE_LIMIT_TIMEOUT = 3 # sec to wait before assuming connection issues and requesting new data again

	def __init__(self, live_config_filename, default_dwell_time):
		
		assert (live_config_filename is not None), 'LS370 config file name was not specified'
		
		# We may have been passed a file name in the default folder
		full_file_path = os.path.join(pyhkd_get_config_dir(), live_config_filename)
		if os.path.exists(live_config_filename):		
			self._cfg_live_filename = live_config_filename
		else:
			self._cfg_live_filename = full_file_path
			
		self._cfg_live = LiveCfg(self._cfg_live_filename)
				
		# This box reads one at a time, so give the option to skip
		self.enabled = [True for i in range(self.NUM_SENSORS)]
		
		# This lets us spend more time at specific channels (in seconds) if we use
		# a manual cycling scheme.  This doesn't affect autocycling.
		# Measure Time + Pause Time = Dwell Time
		self.dwell_time = [default_dwell_time for i in range(self.NUM_SENSORS)]
		
		# Scan to the first channel
		self.currently_reading = 0
		
		self._ls370_lock = threading.Lock()
		self._last_processed = False
		self._last_processed_time = time.time()
		self._last_read = True
		self._last_read_time = time.time()
				
	# Perform the initial config.
	# Overrides AbstractSCPIInstrument.initial_config.
	def initial_config(self):
		
		AbstractSCPIInstrument.initial_config(self)
		
		with self._ls370_lock:
			
			logging.debug("Performing LS370 reconnect config")
			
			# Make sure each channel can have its own settings
			self.send_packet("CHGALL 0")
			
			# Enable reporting of valid reads
			self.send_packet("*SRE 4")
			
			self._fix_live_config() # Run at least once on boot
			self._execute_live_config()
			
			self._mux_update()
			
			# Force a RATE_LIMIT_TIMEOUT before checking for valid data 
			self._last_processed = False
			self._last_processed_time = time.time()
			self._last_read = True
			self._last_read_time = time.time()
		
	# Scan to the currently chosen channel.  Assumes the lock is held.
	def _mux_update(self):
		
		if self.verbose_tx:
			logging.debug("LS370 scanning to index %i (channel %i)" % (self.currently_reading, self.currently_reading + 1))
		
		self.send_packet("SCAN %i,0" % (self.currently_reading + 1))
		self.send_packet("*CLS")
		
	# Safely maps a comma seperated integers to a list of integers.
	# Returns None on failure.
	def _map_comma_str_to_ints(self, str_input, valid_size=None):
		
		try:
			# Attempt to extract the data as a list of integers		
			values = list(map(int,str_input.rstrip().split(',')))
			
			# Make sure it is the right size
			if (valid_size != None) and (len(values) != valid_size):
				raise ValueError()
			
		except:
			logging.error("Error interpreting response from LS370.  Read: " + repr(str_input))
			return None
		
		return values
		
	# Load a json file with the live config.  Expects a list of objects
	# with the following fields: channel, dwell_time, auto_range,
	# excitation, enabled, mode, pause_time, resistance_range.
	# Assumes the lock is held.
	def _execute_live_config(self):
		
		config = self._cfg_live.load()
		
		if config is None:
			logging.error("Unable to load config file: " + str(self._cfg_live_filename))
			self._fix_live_config()
			return
		
		try:
			for c in config:
				i = c.get('channel', None)
				if (i is not None) and (i >= 0) and (i <= 15):
					self._config_channel(i, 
										 c.get('mode', self.MODE_VOLTAGE),
										 c.get('excitation', 3),
										 c.get('resistance_range', 12),
										 c.get('auto_range', False))
					self._config_channel_meta(i, True,
											  c.get('dwell_time', 10),
											  c.get('pause_time', 3))
					self.enabled[i] = c.get('enabled', True)
					self.dwell_time[i] = c.get('dwell_time', 10)
			
			logging.info("Successfully reloaded LS370 live config: " + str(self._cfg_live_filename))	
			
		except (ValueError, TypeError):
			logging.error(str(traceback.format_exc()).rstrip())
			logging.error("Contained error while interpreting config file: " + str(self._cfg_live_filename))
			self._fix_live_config()
	
	# Default channel entry for the live config file
	def _get_default_channel_config(self, index):
		return {"dwell_time": 10,
			"auto_range": True,
			"excitation": 3,
			"enabled": True,
			"mode": self.MODE_VOLTAGE,
			"pause_time": 3,
			"resistance_range": 12,
			"channel": index,
			"name":self.get_channel(index).get("name")}
			
	# Update the live config with current themometer names and ensure 
	# all channels are represented.  Eject any malformed channels.
	# Assumes the lock is held.
	def _fix_live_config(self):
		
		logging.info("Verifying and fixing LS370 live config file: " + str(self._cfg_live_filename))
	
		config = self._cfg_live.load()
		new_config = [self._get_default_channel_config(i) for i in range(16)]	
		
		if config is None or not isinstance(config, list):
			logging.error("Empty or bad LS370 live config file, generating default: " + str(self._cfg_live_filename))
		else:
			# Start from a good ordered default with current names,
			# and then copy over any keys from valid entries in the
			# old config
			for c in config:
				i = c.get('channel', None)
				if (i is not None) and (i >= 0) and (i <= 15):
					for k in list(c.keys()):
						new_config[i][k] = c[k]
					new_config[i]['name'] = self.get_channel(i).get("name")
		
		self._cfg_live.dump(new_config)
			
	# Sets the parameters for a channel.  Assumes the lock is held.
	# Arguments:
	# 	channel - channel ID 0-15 (or -1 for all channels)
	#	mode - excitation mode, either MODE_VOLTAGE or MODE_CURRENT
	#	excitation - excitation level (0-21 for current or 0-11 for voltage, see NAMES_VOLTAGE / NAMES_CURRENT)
	#	resistance_range - resistance read range (0-21, see NAMES_RESISTANCE)
	#	auto_range - if True, auto resistance ranging is enabled (may cause jumps while settling, not recommended)
	#	powered_off - if True, the exicitation is turned off
	def _config_channel(self, channel, mode, excitation, resistance_range, auto_range = False, powered_off = False):
		self.send_packet("RDGRNG %i,%i,%i,%i,%i,%i" % (channel+1, mode, excitation+1, resistance_range+1, int(auto_range), int(powered_off)))

	# Sets the parameters for a channel.  Assumes the lock is held.
	# Arguments:
	# 	channel - channel ID 0-15 (or -1 for all channels)
	#	hw_enabled - True or False, enable or disables the channel in hardware (don't need to do this)
	#	dwell_time - 1-200 integer number of seconds to dwell in autoscanning
	#	pause_time - 3-200 integer number of seconds to pause before looking at data
	#	curve_num - specify the calibration cuver 1-20. 0 is no curve.
	#	temp_co - 0 for negative, 1 for positive
	def _config_channel_meta(self, channel, hw_enabled, dwell_time, pause_time, curve_num = 0, temp_co = 0):
		self.send_packet("INSET %i,%i,%i,%i,%i,%i" % (channel+1, int(hw_enabled), dwell_time, pause_time, curve_num, temp_co+1))
		
	# Handle the main resistance readings
	def handle_resistance(self, chan_index, val):
		
		try:
			# Attempt to extract the value
			extracted_res = float(val)
		except:
			logging.error("Error loading in LS370 channel " + str(chan_index+1) + " resistance.  Read: " + repr(response))
			return	
		
		calib_func = self.get_calib_func(chan_index)

		# Save the values
		self.get_sensor(chan_index, Sensor.TYPE_RESISTANCE).value = extracted_res
		self.get_sensor(chan_index, Sensor.TYPE_TEMPERATURE).value = calib_func(extracted_res)
		
		with self._ls370_lock:
			self._last_read = True
			self._last_read_time = time.time()
		
	# Handle the status byte, which tells us when a valid reading exists
	def handle_status(self, value):
		
		with self._ls370_lock:
			
			self._last_processed = True
			self._last_processed_time = time.time()
			
			try:
				value = int(value)
			except:
				logging.error("Invalid LS370 status byte: " + str(value))
				return
				
			data_ready = (value == 4)
				
			if self.verbose_rx and data_ready:
				logging.debug("LS370 valid data flag is set!")
				
			max_dwell = self.dwell_time[self.currently_reading]
			data_overdue = ((time.time()-self._last_read_time) > max_dwell)
			
			if (self.verbose_rx) and (not data_ready) and (data_overdue):
				logging.debug("Forcing read on overdue LS370 channel (may be open or out of range)")
			
			# If valid data is ready, read it now and move on.  If not,
			# wait for at most the dwell time and then read anyway.
			# (This happens for open/bad sensors, so it is important.)
			if data_ready or data_overdue:
				self._read_current()
				self._last_read_time = time.time()
				self._last_read = False
	
	# Read the current valid value and scan the mux.  Assumed lock is held.
	def _read_current(self):
		
		if self.verbose_tx:
			logging.debug("Reading LS370 mux channel %i" % self.currently_reading)
		
		# Read the resistance for the currently addressed channel.
		# The "c=self.currently_reading" is to freeze in the current value
		cb = lambda x, c=self.currently_reading: self.handle_resistance(c, x)
		self.send_packet("RDGR? %i" % (self.currently_reading + 1), resp_callback=cb)
		self.send_packet("*CLS")
		
		# Find the list of enabled channels
		enabled_indices = np.extract(list(self.enabled), np.arange(self.NUM_SENSORS))
		if len(enabled_indices) == 0:
			return
		
		# Figure out where we are in the enabled channel list
		try:
			cur_index = np.where(enabled_indices == self.currently_reading)[0][0]
		except IndexError:
			cur_index = -1
			
		# Move to the next enabled channel
		cur_index += 1
		if cur_index >= len(enabled_indices):
			cur_index = 0
		next_read = enabled_indices[cur_index]
		if next_read != self.currently_reading:
			self.currently_reading = next_read
			self._mux_update()

	# Code to run when connected with a period of wait_time.
	# Implements AbstractSCPIInstrument.update_connected
	def update_connected(self):
		
		with self._ls370_lock:

			# Check if we need to reload the config file
			if self._cfg_live.wasModified():
				self._execute_live_config()
			
			# Make sure we arent completely disabled
			if np.all(self.enabled == False):
				return
				
			# Check if the last status byte check is still outstanding
			overdue = ((time.time()-self._last_processed_time) >= self.RATE_LIMIT_TIMEOUT)
			if (not self._last_processed or not self._last_read) and (not overdue):
				return
			self._last_processed = False
			
			# Check for valid reading
			self.send_packet("*STB?", resp_callback=self.handle_status)
			
		
class GPIBLakeshore370(AbstractLakeshore370, GPIBSCPIInstrument):
	
	# "wait_time" is the cadence at which to check for valid new data
	def __init__(self, controller, address, channels=[], wait_time=0.5, live_config_filename=None, default_dwell_time=10, **kwargs):
						
		AbstractLakeshore370.__init__(self,
			live_config_filename = live_config_filename,
			default_dwell_time = default_dwell_time)
	
		GPIBSCPIInstrument.__init__(self, controller, address, channels, 
			wait_time = wait_time,
			default_sensor_type = Sensor.MULTI_TYPE_THERM_R, 
			default_calib_func = get_calib('R2T_null'), 
			**kwargs)

class SerialLakeshore370(AbstractLakeshore370, SerialSCPIInstrument):
		
	# "wait_time" is the cadence at which to check for valid new data	
	def __init__(self, port, channels=[], wait_time=0.5, baudrate=9600, live_config_filename=None, default_dwell_time=10, **kwargs):

		AbstractLakeshore370.__init__(self,
			live_config_filename = live_config_filename,
			default_dwell_time = default_dwell_time)

		SerialSCPIInstrument.__init__(self, 
			port, 
			baudrate = baudrate, 
			bytesize=serial.SEVENBITS, 
			parity=serial.PARITY_ODD, 
			stopbits=serial.STOPBITS_ONE,
			pkt_end = '\r\n',
			pkt_start = None,
			channels = channels,
			default_sensor_type = Sensor.MULTI_TYPE_THERM_R, 
			default_calib_func = get_calib('R2T_null'),
			wait_time = wait_time,
			**kwargs)
