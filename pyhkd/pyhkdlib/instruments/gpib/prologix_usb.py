import logging
import time

from pyhkdlib.instruments.serial_instrument import SerialInstrument

class PrologixUSB(SerialInstrument):
	
	BOX_TYPE = 'PROLOGIX'
	NUM_SENSORS = 0
	
	# NOTE that the read_tmo_ms below must be adjusted if SER_ASK_TIMEOUT 
	# is reduced.  Speeding up the timeout of dead asks is appealing,
	# but it causes sync issues due to how slowly some devices respond
	# to things as simple as "*IDN?".  The best thing to do leave the 
	# long timeouts, but detect failed devices and stop sending 
	# frequent requests to them.  This means in the long run the
	# speed should not be limited by this timeout, but during short
	# periods of confusion things have the time they need to settle.
	SER_ASK_TIMEOUT = 1.2 # seconds 
	SER_TX_TIMEOUT = 20  # seconds before dropping TX packets
	
	SER_TX_PAUSE = 0.05 # sec
	
	def __init__(self, port, **kwargs):
		
		self._port = port

		SerialInstrument.__init__(self, port, baudrate = 19200, 
			pkt_end = '\n', pkt_start = None, **kwargs)			
		
	# Implements Instrument.update()
	def update(self):	
		pass
	
	# Implements SerialInstrument.on_reconnect()
	def on_reconnect(self):
		
		logging.debug("Handling Prologix re-connect")
		
		self.purge_bufs()
		
		# Set to command mode
		self.send_packet("++mode 1")
		
		# Turn off auto-reply
		self.send_packet("++auto 0")
		
		# Set read timeout to N milliseconds since the last character
		# was received.  This should be much less than SER_ASK_TIMEOUT
		# to ensure the Prologix gives up before we give up, because
		# we can't have the Prologix send data after we have already
		# purged the ask.  Unfortunately the Prologix does not inform
		# us when its read has timed out (an empty packet or something
		# would be nice).
		self.send_packet("++read_tmo_ms 600")
		
	# Implements SerialInstrument.handle_packet()
	def handle_packet(self, msg):
		msg = msg.strip()
		if len(msg) > 0:
			logging.error("Unexpected packet received by Prologix: " + repr(msg))

	# Send a packet to the specified address on the GPIB bus.  If 
	# resp_callback is not None, then the device is asked to respond
	# and resp_callback is a function called with the response.
	def addr_send_packet(self, address, packet, resp_callback=None):
		
		# If we are sent bytes, convert to a string so we can add to it
		if hasattr(packet, 'decode'):
			packet = packet.decode()
	
		if address not in range(31):
			logging.error("Invalid GPIB address: " + repr(address))
			return
		
		# Switch addresses 
		to_send = '++addr %i\n++clr\n%s' % (address, packet)
		#~ to_send = '++addr %i\n%s' % (address, packet)
		if resp_callback is not None:
			# Send the read command if we are expecting a response
			to_send += '\n++read eoi'
		
		if self.verbose_tx:
			logging.debug("Sending prologix packet: " + repr(to_send))
		
		self.send_packet(to_send, resp_callback)
	
	# Request a read from a device without clearing it or sending 
	# anything. Useful when you need to request data and read it later.
	def addr_read(self, address, resp_callback=None):
		
		to_send = '++addr %i\n++read eoi' % (address)
		
		if self.verbose_tx:
			logging.debug("Sending prologix packet: " + repr(to_send))
		
		self.send_packet(to_send, resp_callback)
		
	
	# Perform a serial poll of the address provided, and return the 
	# status byte.  The status byte is also cleared.
	def serial_poll(self, address, resp_callback):
		to_send = '++spoll %i' % (address)
		self.send_packet(to_send, resp_callback)
