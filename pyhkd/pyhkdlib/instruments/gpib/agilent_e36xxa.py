'''
Read and control an Agilent E36XX
'''

import sys
import time
import logging
import threading
import numpy as np

from pyhkdlib.sensor import Sensor
from .gpib_instrument import AbstractSCPIInstrument, GPIBSCPIInstrument, SerialSCPIInstrument
from pyhkdlib.instruments.voltage_output_mixin import VoltageOutputMixin

# You probably don't want to use this class directly.  See the
# subclasses at the bottom of this file.
		
class AgilentE36XXA(VoltageOutputMixin, GPIBSCPIInstrument):
	
	NUM_SENSORS = 0
	BOX_TYPE = 'E36XXA'
	MIN_VOLTAGE_OUTPUT = []
	MAX_VOLTAGE_OUTPUT = []
	CUR_LIMIT_MAX = []
	OUTPUT_ID = []
	
	# Time to wait for things to settle before resending the instrument
	# config after reconnecting
	RECONFIG_PAUSE = 3 # sec
	
	# If None, then this device has one voltage range.  Otherwise,
	# this is a list containing a list of valid range names for each
	# channel, such as [['HIGH','LOW'],['HIGH','LOW']].  The range is
	# chosen at boot with the "output_range" parameter in the hardware
	# config. The first listed range for each channel is the default
	# used if nothing is given in the config.
	VALID_RANGES = None
	
	# True if the model requires the channel ID in the APPL command
	APPL_NAME = False 

	# Message Available bit in status byte
	STATUS_BIT_MAV = 16
	
	# Time to wait for state timeouts (missed reads, missed serial polls).
	# This should be larger than the packet ask timeout since it might
	# have been sitting in the queue awhile
	AGILENT_STATE_TIMEOUT = 30 # sec
	
	# "device_interface" should be a GpibDeviceInterface.
	def __init__(self, controller, address, channels=[], wait_time=5):
		
		# Speed up the wait time and spread the request load over it.
		# We request either one current or voltage for one sensor each
		# subframe, giving 2*NUM_SENSORS measurements.  Each measurement
		# takes 3 subframes normally (send command, check serial poll,
		# send read request).  We also use the subframes to  check for 
		# voltage updates more frequently.
		self.frame_subsamples = 3 * 2 * self.NUM_SENSORS
		wait_time /= self.frame_subsamples
		self._subframe_num = 0
		
		# The names used work for IDN
		self.IDN_STR = self.BOX_TYPE
		
		self._agilent_lock = threading.Lock()
		self._message_ready = False
		self._waiting_status = False
		self._request_open = False
		self._request_buf = [None] * self.NUM_SENSORS
		self._read_type = Sensor.TYPE_VOLTAGE
		self._read_index = 0
		
		assert self.NUM_SENSORS > 0, "The AgilentE36XXA class is not meant to be used directly, it should be subclassed"
		if self.VALID_RANGES is None:
			assert len(self.MIN_VOLTAGE_OUTPUT) == self.NUM_SENSORS
			assert len(self.MAX_VOLTAGE_OUTPUT) == self.NUM_SENSORS
			assert len(self.CUR_LIMIT_MAX) == self.NUM_SENSORS
		else:
			for chan_id in range(self.NUM_SENSORS):
				assert len(self.RANGE_MAX_VOLTAGE_OUTPUT[chan_id]) == len(self.VALID_RANGES[chan_id])
				assert len(self.RANGE_MIN_VOLTAGE_OUTPUT[chan_id]) == len(self.VALID_RANGES[chan_id])
				assert len(self.RANGE_CUR_LIMIT_MAX[chan_id]) == len(self.VALID_RANGES[chan_id])
				self.MAX_VOLTAGE_OUTPUT = [0] * self.NUM_SENSORS
				self.MIN_VOLTAGE_OUTPUT = [0] * self.NUM_SENSORS
				self.CUR_LIMIT_MAX = [0] * self.NUM_SENSORS
		assert len(self.OUTPUT_ID) == self.NUM_SENSORS
		assert self.RECONFIG_PAUSE < self.DISCONNECTED_TIMEOUT - 2
		
		GPIBSCPIInstrument.__init__(self, controller, address, channels, default_sensor_type = Sensor.MULTI_TYPE_HEATER_I, wait_time=wait_time)	
		
		VoltageOutputMixin.__init__(self, max_voltages = self.MAX_VOLTAGE_OUTPUT, min_voltages = self.MIN_VOLTAGE_OUTPUT, can_set_power=False)		
	
	# Perform the initial config.
	# Overrides AbstractSCPIInstrument.initial_config.
	def initial_config(self):
		
		AbstractSCPIInstrument.initial_config(self)

		# Set the output range if relevant
		if self.VALID_RANGES is not None:
			
			pkt = ''
			
			for chan_id in self.sensor_ids:
				
				rng = self.get_channel(chan_id).get("output_range", self.VALID_RANGES[chan_id][0])
				rng = str(rng).upper()
				
				# Update MIN_VOLTAGE_OUTPUT, MAX_VOLTAGE_OUTPUT, CUR_LIMIT_MAX
				if rng not in self.VALID_RANGES[chan_id]:
					raise ValueError("Invalid output_range '%s' for %s PSU.  Allowed values: %s" % (rng,self.BOX_TYPE,str(self.VALID_RANGES)))
			
				rng_id = self.VALID_RANGES[chan_id].index(rng)
				self.MAX_VOLTAGE_OUTPUT[chan_id] = self.RANGE_MAX_VOLTAGE_OUTPUT[chan_id][rng_id]
				self.MIN_VOLTAGE_OUTPUT[chan_id] = self.RANGE_MIN_VOLTAGE_OUTPUT[chan_id][rng_id]
				self.CUR_LIMIT_MAX[chan_id] = self.RANGE_CUR_LIMIT_MAX[chan_id][rng_id]
				
				logging.info("Changing %s PSU output range to %s" % (self.BOX_TYPE,rng)) 
				
				pkt += self._select_output(chan_id)
				pkt += "VOLTAGE:RANGE %s\n" % rng
				
			self.send_packet(pkt)
		
	# Packet string to select an output channel
	def _select_output(self, chan_id):
			
		if self.NUM_SENSORS > 1:
			return "INST:SEL " + self.OUTPUT_ID[chan_id] + "\n"
		else:
			return ""
	
	# Handle a current or voltage value returned
	def handle_value(self, chan_id, sensor_type, data_str):
		
		#~ logging.debug("Agilent %s value: chan %s, type %s, val %s " % (self.BOX_TYPE, chan_id, sensor_type, data_str))
		
		with self._agilent_lock:
			self._message_ready = False
		
		try:
			val = float(data_str)
		except:
			logging.error("Error loading in " + self.SPECIFIC_DEVICE_ID + " value. Read: " + repr(data_str)) 
			return
					
		self.get_sensor(chan_id, sensor_type).value = val
		
		# Don't change voltage on boot
		if sensor_type == Sensor.TYPE_VOLTAGE:
					
			sen = self.get_sensor(chan_id, Sensor.TYPE_TARGET_VOLTAGE)
			
			if sen.value is None or not np.isfinite(sen.value):
				sen.value = val
	
	# Handle the status bit returned form a serial poll
	def handle_status(self, val):
		
		with self._agilent_lock:
			
			self._waiting_status = False
		
			try:
				val = int(val)
			except:
				logging.error("Invalid status byte from %s: %s" % (self.SPECIFIC_DEVICE_ID, repr(val)))
				return
			
			if self.verbose_rx:
				logging.debug("Status byte from %s: %s" % (self.SPECIFIC_DEVICE_ID, repr(val)))	
			
			# Look for the message available
			if not (val & self.STATUS_BIT_MAV):
				return
			
			if not self._request_open:
				logging.error("Status byte received with no pending data request on %s" % (self.SPECIFIC_DEVICE_ID))
				return 
				
			if not self._message_ready:
				self._message_ready = True
				self._message_ready_time = time.time()
			elif self.verbose_rx:
				logging.debug("Repeated MAV report from %s" % (self.SPECIFIC_DEVICE_ID))
	
	# Code to run when connected with a period of wait_time.
	# Implements AbstractSCPIInstrument.update_connected
	def update_connected(self):
		
		# Process targets at a higher rate (frame_subsamples times faster)
		# to give finer control and better responsiveness
		self.process_voltage_targets()
		
		with self._agilent_lock:
			
			# Check if we are waiting to do a read
			if self._request_open:
				
				# Check if we are ready to do the read
				if self._message_ready:
					
					# The "c=chan_id" is to freeze in the current value
					cb = lambda s, c=self._read_index, t=self._read_type: self.handle_value(c, t, s)
					self.request_read(resp_callback=cb)
					self._request_open = False
					
				else:
					
					if (time.time() - self._request_open_time) > self.AGILENT_STATE_TIMEOUT:
						logging.debug("Timeout for request_open on %s" % (self.SPECIFIC_DEVICE_ID))
						self._request_open = False
						
					# Send a serial poll if we aren't waiting on one already
					if not self._waiting_status:
						self._waiting_status = True
						self.request_status(self.handle_status)
			else:
				
				# Make sure we are not still waiting on a read
				if self._message_ready:
					
					if (time.time() - self._message_ready_time) > self.AGILENT_STATE_TIMEOUT:
						logging.debug("Timeout for message_ready on %s" % (self.SPECIFIC_DEVICE_ID))
						self._message_ready = False
					else:
						if self.verbose_rx:
							logging.debug("Waiting for read to complete on %s" % (self.SPECIFIC_DEVICE_ID))
						return
				
				self._request_open = True
				self._request_open_time = time.time()
				
				# Look for new voltages to set, otherwise request
				# the next scheduled value
				for i in range(self.NUM_SENSORS):
					if self._request_buf[i] is not None:
						self.send_packet(self._request_buf[i])
						self._request_buf[i] = None
						self._read_type = Sensor.TYPE_VOLTAGE
						self._read_index = i
						break
				else:
					# Request data as a pure send instead of an ask.
					# We will ask for the result later once the serial
					# poll indicates it is ready.
					if self._read_type == Sensor.TYPE_VOLTAGE:
						self._read_type = Sensor.TYPE_CURRENT
						self.send_packet(self._select_output(self._read_index) + 'MEAS:CURR?')
					else:
						self._read_index = (self._read_index + 1) % self.NUM_SENSORS
						self._read_type = Sensor.TYPE_VOLTAGE
						self.send_packet(self._select_output(self._read_index) + 'VOLT?')
				
	# Implements VoltageOutputMixin.set_voltage
	# Set output "output" to voltage "voltage".  "output" is the output index (0,1,2), and "voltage" must be a valid float.
	def set_voltage(self, output, voltage):
		
		if output >= self.NUM_SENSORS:
			raise ValueError("Agilent power supply output out of range")
				
		name_str =  ""
		if self.APPL_NAME:
			name_str = self.OUTPUT_ID[output] + ", "
		
		# Per-channel current limit
		current_limit = self.get_channel(output).get("current_limit", self.CUR_LIMIT_MAX[output])
		current_limit = min(current_limit, self.CUR_LIMIT_MAX[output])
		current_limit = max(current_limit, 0)
		
		pkt = self._select_output(output)
		pkt += "APPL " + name_str + str(voltage) + ", %s\n" % (current_limit)
		pkt += "OUTP ON\nVOLT?"
		
		with self._agilent_lock:
			
			# Check if this is already being requested
			if self._request_buf[output] == pkt:
				return
				
			logging.debug('[' + str(id(self)) + '] Setting Output ' + str(output) + ' to ' + str(voltage) + 'V') 
			
			if self._request_buf[output] is not None:
				logging.debug("Overwriting unserved output %i change request on %s: %s" % (output, self.SPECIFIC_DEVICE_ID, repr(self._request_buf[output])))
			
			# Store the packet to send later
			self._request_buf[output] = pkt 
		
# Make sure SYSTem:REMote is called in initial_config for serial versions

class AgilentE3631A(AgilentE36XXA):
	
	NUM_SENSORS = 3
	BOX_TYPE = 'E3631A'
	MAX_VOLTAGE_OUTPUT = [6, 25, 0]
	MIN_VOLTAGE_OUTPUT = [0, 0, -25]
	OUTPUT_ID = ['P6V', 'P25V', 'N25V']
	APPL_NAME = True
	CUR_LIMIT_MAX = [5.0, 1.0, 1.0]
	
class AgilentE3632A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3632A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[30,15]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[4,7]]
	
class AgilentE3633A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3633A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[20,8]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[10,20]]
	
class AgilentE3634A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3634A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[50,25]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[4,7]]

class AgilentE3640A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3640A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[20,8]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[1.5,3.0]]
		
class AgilentE3641A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3641A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[60,35]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[0.5,0.8]]

class AgilentE3642A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3642A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[20,8]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[2.5,5.0]]
		
class AgilentE3643A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3643A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[60,35]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	CRANGE_UR_LIMIT_MAX = [[0.8,1.4]]
	
class AgilentE3644A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3644A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[20,8]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[4.0,8.0]]
		
class AgilentE3645A(AgilentE36XXA):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'E3645A'
	VALID_RANGES = [['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[60,35]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0]]
	OUTPUT_ID = ['OUT1']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[1.3,2.2]]
	
class AgilentE3646A(AgilentE36XXA):
	
	NUM_SENSORS = 2
	BOX_TYPE = 'E3646A'
	VALID_RANGES = [['HIGH','LOW'],['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[20,8],[20,8]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0], [0,0]]
	OUTPUT_ID = ['OUT1', 'OUT2']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[1.5,3.0], [1.5,3.0]]
		
class AgilentE3647A(AgilentE36XXA):
	
	NUM_SENSORS = 2
	BOX_TYPE = 'E3647A'
	VALID_RANGES = [['HIGH','LOW'],['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[60,35],[60,35]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0], [0,0]]
	OUTPUT_ID = ['OUT1', 'OUT2']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[0.5,0.8], [0.5,0.8]]
		
class AgilentE3648A(AgilentE36XXA):
	
	NUM_SENSORS = 2
	BOX_TYPE = 'E3648A'
	VALID_RANGES = [['HIGH','LOW'],['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[20,8],[20,8]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0], [0,0]]
	OUTPUT_ID = ['OUT1', 'OUT2']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[2.5,5.0], [2.5,5.0]]

class AgilentE3649A(AgilentE36XXA):
	
	NUM_SENSORS = 2
	BOX_TYPE = 'E3649A'
	VALID_RANGES = [['HIGH','LOW'],['HIGH','LOW']]
	RANGE_MAX_VOLTAGE_OUTPUT = [[60,35],[60,35]]
	RANGE_MIN_VOLTAGE_OUTPUT = [[0,0], [0,0]]
	OUTPUT_ID = ['OUT1', 'OUT2']
	APPL_NAME = False
	RANGE_CUR_LIMIT_MAX = [[0.8,1.4], [0.8,1.4]]

