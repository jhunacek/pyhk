import time
import logging
import serial

from pyhkdlib.sensor import Sensor
from .serial_instrument import SerialInstrument
from calib.helpers import get_calib

# Alcatel Adixen ACP40/28 via a RS485-RS232 converter
class AdixenACP(SerialInstrument):
	
	NUM_SENSORS = 0
	BOX_TYPE = 'ADIXENACP'
			
	def __init__(self, port, channels=[], wait_time=1, baudrate = 9600, address = 0, name_prefix = 'ACP', **kwargs):

		assert len(channels) == 0, 'The Adixen ACP config uses "name_prefix" instead of "channels"'
		
		# Generate the list of channels
		p = str(name_prefix)
		channels.append({"id": "motor", "name": p+" Motor", "type": [Sensor.TYPE_FREQUENCY, Sensor.TYPE_POWER]})
		channels.append({"id": "converter", "name": p+" Converter", "type": Sensor.TYPE_TEMPERATURE})
		channels.append({"id": "status", "name": p+" Status", "type": Sensor.TYPE_NUMBER})
		channels.append({"id": "fault", "name": p+" Fault", "type": Sensor.TYPE_NUMBER})
		self.NUM_SENSORS = len(channels)
		
		SerialInstrument.__init__(self, 
			port, 
			baudrate = baudrate, 
			bytesize=serial.EIGHTBITS, 
			parity=serial.PARITY_NONE, 
			stopbits=serial.STOPBITS_ONE,
			pkt_end = '\r\n',
			pkt_start = '#',
			channels = channels,
			verbose_fail = False,
			**kwargs)
		
		self._address = int(address)
		self.wait_time = wait_time			
		
	def handle_packet(self, msg):
		
		try:
			response_split = msg.rstrip().split(",")
			status = int(response_split[1])
			fault = int(response_split[2])
			motor_speed = float(response_split[4]) / 60.0 
			motor_power = float(response_split[5])
			conv_temp = float(response_split[7]) + 273.15
		except (TypeError, AttributeError, ValueError):
			logging.error("Error loading status from Adixen ACP pump.  Read " + str(msg) )			
			self.purge_bufs()
			return
						
		self.get_sensor('motor', Sensor.TYPE_FREQUENCY).value = motor_speed
		self.get_sensor('motor', Sensor.TYPE_POWER).value = motor_power
		self.get_sensor('converter', Sensor.TYPE_TEMPERATURE).value = conv_temp
		self.get_sensor('status', Sensor.TYPE_NUMBER).value = status
		self.get_sensor('fault', Sensor.TYPE_NUMBER).value = fault
				
	def update(self):
		
		# Check if certain amount of time has passed since something was read
		if ((time.time() - self.last_update_time) >= self.wait_time):
			
			# Read status
			self.send_packet('%03iSTA' % self._address) 
			
			self.last_update_time = time.time() # (We still want this even if we fail, so it gives time before retrying)




		
