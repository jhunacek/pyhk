import os
import logging
import time
import serial
import math
import threading
import random

from pyhkdlib.sensor import Sensor
from pyhkdlib.instruments.instrument import Instrument

class SimulatedData(Instrument):

	# "wait_time" is a value in sec.  All sensors will be assigned a
	# new random value at this rate.
	def __init__(self, channels, wait_time, delta_sync=None, **kwargs):
		
		# Allow however many channels are specified	
		self.NUM_SENSORS = len(channels)
		
		Instrument.__init__(self, channels, wait_time=wait_time, **kwargs)
		
		assert self.wait_time > 0, "The wait_time for simulated values must be >0 sec"
		
		self._next_sync = None
		if delta_sync is not None:
			self._delta_sync = delta_sync
			self._next_sync = 0
		
		logging.info("Simulating %i sensors at %0.3f Hz" % (self.NUM_SENSORS, 1/self.wait_time))
		
		self._thread = threading.Thread(target = self._loop_sim, name="SimData:" + str(id(self)))
		
		# Don't let the threads keep the program alive
		self._thread.daemon = True
		
		for k, s in self._sensors.items():
			s.value = random.uniform(0, 300)
	
		self._threads_running = True 
		self._thread.start()
		
	def __del__(self):
		try:
			self.close()		
		except:
			# We are dying anyway, lets not pollute the terminal
			# with errors that confuse people
			pass
			
	# Shut down the thread
	def close(self):
		self._threads_running = False
		self._thread.join()

	# Run forever simulating data
	def _loop_sim(self):	
									
		while (self._threads_running):
			
			t = time.time()
			sync = self._next_sync
			
			for k, s in self._sensors.items():
				v = s.value + random.uniform(-0.1, 0.1)
				s.set_value(v, t, sync)
			
			if self._next_sync is not None:
				self._next_sync += self._delta_sync
			
			time.sleep(self.wait_time)
			
			


