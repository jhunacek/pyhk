import time
import logging
import serial

from pyhkdlib.sensor import Sensor
from .serial_instrument import SerialInstrument
from calib.helpers import get_calib

# MKS pressure sensors, including 972B DualMag and 902B
class MKSPressureSensor(SerialInstrument):
	
	NUM_SENSORS = 1
	BOX_TYPE = 'MKSPRESSURE'
			
	def __init__(self, port, channels=[], wait_time=1, baudrate = 9600, **kwargs):

		SerialInstrument.__init__(self, 
			port, 
			baudrate = baudrate, 
			bytesize=serial.EIGHTBITS, 
			parity=serial.PARITY_NONE, 
			stopbits=serial.STOPBITS_ONE,
			pkt_end = ';FF',
			pkt_start = '@',
			channels = channels,
			default_sensor_type = Sensor.TYPE_PRESSURE, 
			verbose_fail = False,
			**kwargs)
		
		self.wait_time = wait_time			
		
	def handle_packet(self, msg):
		
		try:
			response_split = msg.rstrip().split("ACK")
			val = float(response_split[1])
		except (TypeError, AttributeError, ValueError):
			logging.error("Error loading from MKS pressure sensor.  Read " + str(msg) )			
			self.purge_bufs()
			return
						
		self.get_sensor(0, Sensor.TYPE_PRESSURE).value = val
				
	def update(self):
		
		# Check if certain amount of time has passed since something was read
		if ((time.time() - self.last_update_time) >= self.wait_time):
			
			if self.verbose_tx:
				logging.debug("Requesting pressure from MKS sensor")
				
			# Read pressure
			self.send_packet('254PR4?') # Main sensor, scientific notation
			
			self.last_update_time = time.time() # (We still want this even if we fail, so it gives time before retrying)




		
