import os
import logging
import time
import serial
import math
import threading
import sys
import ssl

import paho.mqtt.client as mqtt

from pyhkdlib.sensor import Sensor
from pyhkdlib.instruments.instrument import Instrument

# Subscribes to MQTT topics
class MQTTListener(Instrument):
	
	BOX_TYPE = 'MQTT'
	
	# channels: typical array of channels, but each includes a "topic" 
	#		 	field that is subscribed to
	# patterns: an object with two fields, described below
	# 	"channels": a list of channels (with "topic" fields) 
	# 	"names":  	
	def __init__(self, patterns=[], channels=[], address="127.0.0.1", 
		port=1883, clientid="pyhkd", username=None, password=None,
		willtopic=None, protocol=mqtt.MQTTv311, tls=None, 
		verbose=False, **kwargs):
		
		self._connected = False
		self._threads_running = False
		self._verbose = verbose
		
		# Convert device patterns into channels
		for p in patterns:
			
			try:
				pnames = p["names"]
				pchans = p["channels"]
			except:
				sys.exit('Configuration Error: Each MQTT pattern must specify the fields "names" and "channels"')
			
			for k, v in pnames.items():
				for c in pchans:
					newchan = c.copy()
					if 'topic' not in newchan.keys() or 'name' not in newchan.keys():
						sys.exit('Configuration Error: Each MQTT channel pattern must specify a "topic" and a "name"')
					if '%s' not in newchan['topic'] or '%s' not in newchan['name']:
						sys.exit('Configuration Error: Each MQTT channel pattern must use "%s" to indicate the replacement location in the values for "topic" and "name"')
					newchan['topic'] %= k
					newchan['name'] %= v
					if self._verbose:
						logging.debug("Adding MQTT pattern-based channel: " + str(newchan))
					channels.append(newchan)
			
		# This object allows as many sensors as provided in the config
		self.NUM_SENSORS = len(channels)
		
		self.VALID_CHAN_KEYS += ['topic','stringdelim','stringindex']
		self.REQUIRED_CHAN_KEYS += ['topic']
		
		Instrument.__init__(self, channels, **kwargs)
		
		self._address = address
		self._port = port
		self._clientid = clientid
		self._username = username
		self._password = password
		self._willtopic = willtopic

		self._mqttclient = mqtt.Client(client_id=self._clientid, clean_session=True, protocol=protocol)
		self._mqttclient.on_log = self._mqttclient_logger
		
		# Infer from the port if it isn't provided
		if tls is None:
			tls = True
			if port == 1883:
				tls = False
		
		if tls:
			self._mqttclient.tls_set()

		# Password can be None
		if self._username is not None:
			self._mqttclient.username_pw_set(username=self._username, password=self._password)
		
		self._mqttclient.on_connect = self.mqtt_on_connect
		self._mqttclient.on_message = self.mqtt_on_message_unhandled
		
		# Connect in another thread in case it takes awhile
		self._threads_running = True
		self._thread_conn = threading.Thread(target = self._loop_mqtt_connect, name="MQTT Connect (%s)" % (self._address,))
		self._thread_conn.daemon = True # Don't let the thread keep the program alive
		self._thread_conn.start()
	
	# Clean up
	def __del__(self):
		
		if self._connected:
			self._mqttclient.disconnect()
			
		if self._threads_running:
			self._threads_running = False
			self._thread_conn.join()
	
	# Handle log messages from within the paho library.  Note that 
	# any exceptions generated in callback functions like on_connect
	# are caught and end up here, so make sure to display them.
	def _mqttclient_logger(self, client, userdata, level, buf):
		
		std_lvl = mqtt.LOGGING_LEVEL[level]
		if std_lvl > logging.DEBUG:
			logging.log(std_lvl, buf)
					
	# Re-subscribe
	def mqtt_resub(self):

		logging.info("Subscribing to %i MQTT topics" % (len(self.sensor_ids)))
	
		for cid in self.sensor_ids:
			
			topic = self.get_channel(cid)['topic']
				
			# Wrap the default handler format to make things easier
			def handler(client, userdata, msg, cid=cid): 
				self.new_data(cid, msg.payload)
			
			self._mqttclient.subscribe(topic, 2)
			self._mqttclient.message_callback_add(topic, handler)
			
	# Handle new messages
	def new_data(self, channel_id, value):
		
		# For error messages
		topic = self.get_channel(channel_id)['topic']
		
		try:
			value = value.decode() # bytes to str
		except UnicodeDecodeError:
			logging.error("Invalid (non-string) value received on MQTT topic %s: %s" % (topic, repr(value)))
		
		# Optionally support basic arrays with multiple values by
		# letting a delimiter and index be specified.  This also can
		# handle messages like "4 seconds", where a delimiter of " "
		# and an index of 0 produces the value "4", which can be 
		# properly digested			
		index = int(self.get_channel(channel_id).get('stringindex', 0))
		delim = self.get_channel(channel_id).get('stringdelim', None)
		if delim is not None:
			delim = str(delim)
		
		try:	
			value = float(value.split(delim)[index])
		except:
			logging.error("Invalid or non-numeric value received on MQTT topic %s: %s" % (topic, str(value)))
			return
		
		if self._verbose:
			logging.debug("Valid value received on MQTT topic %s: %s" % (topic, str(value)))
		
		c_types = self.get_channel_types(channel_id)
		
		# See if we need to apply calibration
		if len(c_types) == 1:
			self.get_sensor(channel_id, c_types[0]).value = value
		elif len(c_types) == 2:
			calib = self.get_calib_func(channel_id)
			self.get_sensor(channel_id, c_types[0]).value = value
			self.get_sensor(channel_id, c_types[1]).value = calib(value)
		else:
			logging.error("Unsure how to handle MQTT sensor on topic %s with %i types: %s" % (topic, len(c_types), str(c_types)))
							
	# Callback
	def mqtt_on_connect(self, client, userdata, flags, rc):
		
		logging.info("MQTT client connection returned result '" + mqtt.connack_string(rc) + "'")
		
		if self._willtopic is not None:
			logging.info("Setting MQTT LWT")
			self._mqttclient.will_set(self._willtopic, payload="false", qos=2, retain=True)
			self._mqttclient.publish(self._willtopic, payload="true", qos=2, retain=True)

		self.mqtt_resub()
	 
	# Callback
	def mqtt_on_message_unhandled(self, client, userdata, message):
		logging.error("Received unhandled message: " + str(message.topic) + ' ' + str(message.payload))

	# Run forever, connects to MQTT server
	def _loop_mqtt_connect(self):	

		
		while self._threads_running and not self._connected:
			try:
				self._mqttclient.connect(self._address, port=self._port, keepalive=60)
				self._connected = True
			except OSError:
				logging.error("MQTT connection failed (%s:%i), retrying" % (self._address, self._port))
				time.sleep(10)
		
		if self._threads_running:
			logging.info("MQTT connected, loop starting")							
			self._mqttclient.loop_forever()
			
		logging.info("MQTT thread ending")

	

