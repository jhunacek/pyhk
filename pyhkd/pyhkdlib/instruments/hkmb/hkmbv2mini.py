import logging

from .hkmbv2 import HKMBv2

class HKMBv2Mini(HKMBv2):
	
	BOX_TYPE = 'HKMBv2Mini'

	MAX_NUM_CARDS = 1
