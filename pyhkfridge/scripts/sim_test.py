# TEST PLAYGROUND FOR VERIFYING FUNCTIONALIY, DO NOT USE



from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		return [self.step0, self.step1, self.step2, lambda:self.step0()]
		
	def step0(self):
		self.log('step 0!')
		self.move_to_next_step()
		
	def step1(self):
		self.log('step 1!')
		self.move_to_next_step()
		
	def step2(self):
		self.log('step 2!')
		self.log(self.get_temperature('3He Cond'))
		self.log(self.get_voltage('IC P'))
		self.log(self.get_voltage('IC HS'))
		self.set_voltage('IC HS', 1.51)
		self.show_status("blah")
		self.move_to_next_step()
		
	def step4(self):
		self.log('step 4!')
		self.move_to_next_step()
	