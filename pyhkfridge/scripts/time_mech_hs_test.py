

import datetime
import numpy as np
import time
import subprocess

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	HEATER_NAME = "Mech HS Heater"

	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step_heater_off,
				self.step_pause,
				self.step_heater_on,
				self.step_pause,
				self.script_restart]
				
	@pause_cycle(duration_seconds = 25*60)
	def step_pause(self, time_remaining="?"):
		self.show_status('Waiting 25 min...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
	
	def step_heater_on(self):
		self.show_status("Turning heater on")
		self.set_voltage(self.HEATER_NAME, 10)		
		self.move_to_next_step()
	
	def step_heater_off(self):
		self.show_status("Turning heater off")
		self.set_voltage(self.HEATER_NAME, 0)		
		self.move_to_next_step()
	
