# TIME Pump Heat Dump Test v1.0

# 2017-01-12 JRH - Initial code



import datetime
import numpy as np
import time

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):

	PUMP_THERMOMETER = 'Pump 1'
	PUMP_HEATER = 'Pump 1 Heater'
	PUMP_POWER = 100e-3
	PUMP_TARGET = 40
	HS_HEATER = 'HS 1 Heater'
	HS_POWERS = np.arange(0, 0.51e-3, .05e-3)
	WAIT_TIME = 2*3600 # Seconds
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2]
	
	# Initialize this run
	def step0(self):
		
		self.show_status('Starting ' + str(self.PUMP_HEATER) + ' dump tests.')
		self.next_power_index = 0
		self.waiting = False
		self.show_status('Heating the pump...')
		self.set_power(self.PUMP_HEATER, self.PUMP_POWER)
		self.move_to_next_step()
		
		
	# Heat the pump
	def step1(self):	
		self.show_status('Waiting for pump to warm to ' + str(self.PUMP_TARGET) + '...', add_to_log = False)
		
		if self.get_temperature(self.PUMP_THERMOMETER) > self.PUMP_TARGET:
			self.show_status('Pump is hot, turning power off.')
			self.set_power(self.PUMP_HEATER, 0)
			self.move_to_next_step()
		
	# HS powers
	def step2(self):
		
		if not self.waiting:
	
			next_power = self.HS_POWERS[self.next_power_index]
			
			self.show_status('Putting ' + str(next_power*1000) + ' mW on the HS')
			self.set_power(self.HS_HEATER, next_power)
			
			self.wait_start_time = time.time()
			self.waiting = True 
			self.next_power_index += 1
			if self.next_power_index >= len(self.HS_POWERS):
				self.move_to_next_step()
			
			
		time_left = self.WAIT_TIME - (time.time() - self.wait_start_time)
		if time_left <= 0:
			self.waiting = False
		else:
			self.show_status('Waiting, ' + str(round(time_left,2)) + ' sec remaining...', add_to_log = False)
		
		
