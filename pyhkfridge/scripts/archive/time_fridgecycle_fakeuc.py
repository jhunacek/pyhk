# TIME Fridge Cycle With Fake UC Load



import time

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	CONFIG_FILE = './FridgeScripts/FridgeCycle_TIME.conf'
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4,
				self.step5,
				self.step6,
				self.step7,
				self.step8,
				self.step9,
				self.step10,
				self.step11,
				self.step12,
				self.step13,
				self.step14,
				self.step15]
		
	#########  Fridge Steps  #########  
	
	# Initialize the cycle
	def step0(self):
		self.show_status('Initializing fridge cycle...')
		
		# Turn all heaters off for both fridges
		self.use_config_section('Fridge 1')
		self.disable_all_heaters()
		self.use_config_section('Fridge 2')
		self.disable_all_heaters()

		self.current_fridge = 'Fridge 1'
		self.move_to_next_step()
		
	# Initialize this particular iteration
	def step1(self):
		self.show_status('Cycling ' + self.current_fridge)
		self.use_config_section(self.current_fridge)
		self.disable_all_heaters()
		self.move_to_next_step()
	
	# Wait for the Evap-mK and Pump-4K switches to cool
	def step2(self):
		
		HS_PUMP_4K = self.get_config('NAME_HS_PUMP_4K')
		HS_EVAP_mK = self.get_config('NAME_HS_EVAP_mK')
		T_COOL_TARGET_HS = self.get_config('T_COOL_TARGET_HS')
		
		self.show_status('Waiting for the Evap-mK and Pump-4K heatswitches to cool to ' + str(T_COOL_TARGET_HS) + ' K...', add_to_log = False)
		
		if (self.get_temperature(HS_PUMP_4K) < T_COOL_TARGET_HS) and (self.get_temperature(HS_EVAP_mK) < T_COOL_TARGET_HS):
			self.show_status('Evap-mK and Pump-4K heatswitches cooled to to ' + str(T_COOL_TARGET_HS) + ' K.')
			self.move_to_next_step()
					
	# Heat up the pump and connect Evap to 1K
	def step3(self):	
		
		HS_EVAP_1K_HTR = self.get_config('NAME_HS_EVAP_1K_HTR')
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		P_HEAT_PUMP_HTR = self.get_config('P_HEAT_PUMP_HTR')
		P_HEAT_HS_EVAP_1K = self.get_config('P_HEAT_HS_EVAP_1K')
		
		self.set_power(HS_EVAP_1K_HTR,  P_HEAT_HS_EVAP_1K)
		self.show_status('Heating up the Evap to 1K heat switch.')
		
		self.set_power(PUMP_HTR, P_HEAT_PUMP_HTR)
		self.show_status('Putting ' + str(P_HEAT_PUMP_HTR) + ' mW on the pump heater.')
		
		self.move_to_next_step()
			
	# Wait for the pump to warm up, then turn it off
	@fail_on_timeout(timeout_hours=2.0)
	def step4(self):
		
		PUMP = self.get_config('NAME_PUMP')
		T_WARM_TARGET_PUMP = self.get_config('T_WARM_TARGET_PUMP')
		
		self.show_status('Waiting for the pump to warm to ' + str(T_WARM_TARGET_PUMP) +' K...', add_to_log = False)
		
		if self.get_temperature(PUMP) > T_WARM_TARGET_PUMP:		
			self.show_status('Pump has warmed to ' + str(T_WARM_TARGET_PUMP) + ' K.')
			self.move_to_next_step()
			
	# Turn off the pump
	def step5(self):
					
		PUMP_HTR = self.get_config('NAME_PUMP_HTR')
		
		self.output_off(PUMP_HTR)
		self.show_status('Pump heater off.')
		
		self.move_to_next_step()

	# Wait for Evap to cool down
	@skip_on_timeout(timeout_hours=2.0)
	def step6(self):
		
		T_COOL_TARGET_EVAP = self.get_config('T_COOL_TARGET_EVAP_CONDENSE')
		
		self.show_status('Waiting for Evap to cool to ' + str(T_COOL_TARGET_EVAP) + ' K...', add_to_log = False)
		
		EVAP = self.get_config('NAME_EVAP')
		
		if self.get_temperature(EVAP) < T_COOL_TARGET_EVAP:
			self.show_status('Evap has cooled to ' + str(T_COOL_TARGET_EVAP) + ' K')
			self.move_to_next_step()
			
	# Disengage the Evap-1K heat switch 
	def step7(self):
			
		HS_EVAP_1K_HTR = self.get_config('NAME_HS_EVAP_1K_HTR')
		
		self.output_off(HS_EVAP_1K_HTR)
		self.show_status('Evap-1K HS heater turned off.')
		
		self.move_to_next_step()
	
	# Wait for the Evap-1K HS to cool, then start pumping
	def step8(self):
		
		T_COOL_TARGET_HS = self.get_config('T_COOL_TARGET_HS')
		HS_EVAP_1K = self.get_config('NAME_HS_EVAP_1K')
		
		self.show_status('Waiting for the Evap-1K HS to cool to ' + str(T_COOL_TARGET_HS) + ' K...', add_to_log = False)
		
		if self.get_temperature(HS_EVAP_1K) < T_COOL_TARGET_HS:
			self.show_status('Evap-1K HS cooled to ' + str(T_COOL_TARGET_HS) + ' K.')
			self.move_to_next_step()
			
	# Start pumping
	def step9(self):

		P_HEAT_HS_PUMP_4K = self.get_config('P_HEAT_HS_PUMP_4K')
		HS_PUMP_4K_HTR = self.get_config('NAME_HS_PUMP_4K_HTR')
		
		self.set_power(HS_PUMP_4K_HTR, P_HEAT_HS_PUMP_4K)
		self.show_status('Heating up the Pump to 4K heat switch.')
		
		self.move_to_next_step()
			
	# Wait for Evap to cool
	def step10(self):
		
		EVAP = self.get_config('NAME_EVAP')
		T_COOL_TARGET_EVAP = self.get_config('T_COOL_TARGET_EVAP_FINAL')
		
		self.show_status('Waiting for Evap to cool to ' + str(T_COOL_TARGET_EVAP) + ' K...', add_to_log = False)
		
		if self.get_temperature(EVAP) < T_COOL_TARGET_EVAP:
			self.show_status('Evap has cooled to ' + str(T_COOL_TARGET_EVAP) + ' K.')			
			self.move_to_next_step()
			
	# Connect Evap to the mK stage
	def step11(self):
			
		HS_EVAP_mK_HTR = self.get_config('NAME_HS_EVAP_mK_HTR')
		P_HEAT_HS_EVAP_mK = self.get_config('P_HEAT_HS_EVAP_mK')
		
		self.set_power(HS_EVAP_mK_HTR,  P_HEAT_HS_EVAP_mK)
		self.show_status('Heating up the Evap to 300 mK heat switch.')
		
		self.move_to_next_step()
		
	# Begin the next iteration
	def step12(self):
		
		# Change fridges if needed
		if self.current_fridge == 'Fridge 1':
			self.show_status('Starting Fridge 2...')
			self.current_fridge = 'Fridge 2'
			self.move_to_step(1)
		else:
			self.show_status('Starting UC Sim...')
			self.current_fridge = 'Fridge 1'
			self.move_to_next_step()
	
	# Turn on UC cycle power
	def step13(self):
		
		self.show_status('Setting fake UC power') 
		self.set_power('300mK Heater', 0.150)
		self.wait_time_sec = 2 * 60 * 60
		self.wait_time_start = time.time()		
		self.move_to_next_step()
	
	# Wait
	def step14(self):
		
		time_remaining_sec = (self.wait_time_sec - (time.time() - self.wait_time_start))
		
		if time_remaining_sec <= 0:
			self.move_to_next_step()
		else:
			self.show_status('Waiting for fake UC cycle to finish (' + str(round(time_remaining_sec/3600,2)) + ' hr remaining)', add_to_log=False)

	# Turn on UC cycle base power
	def step15(self):
			
		self.set_power('300mK Heater', 0.050)
		self.wait_time_sec = 12 * 60 * 60
		self.wait_time_start = time.time()	
		self.show_status('Leaving 50 uW base load on and finishing cycle') 	
		self.move_to_next_step()
			
	#########  Event Hooks  ######### 
			
	# A function that is called when the fridge cycle fails.  Use it to set things to a safe state.
	def failure_cleanup(self):
		self.disable_all_heaters()
		
	#########  Local Helper Functions  ######### 
		
	def disable_all_heaters(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.output_off('300mK Heater')
		self.output_off('Evap 1 Heater')
		self.output_off('Evap 2 Heater')
		self.output_off(self.get_config('NAME_HS_PUMP_4K_HTR'))
		self.output_off(self.get_config('NAME_HS_EVAP_1K_HTR'))
		self.output_off(self.get_config('NAME_HS_EVAP_mK_HTR'))
		self.output_off(self.get_config('NAME_PUMP_HTR'))
		self.output_off(self.get_config('NAME_EVAP_HTR'))
		
		self.show_status('All voltages outputs for this fridge have been set to 0.')
		
		
		
