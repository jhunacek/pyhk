# Shortkeck Fridge Cycle v0.1.3

# 2014-10-16 JRH - Initial code written based on Animesh/Zak/Jon cycle
# 2015-03-23 JRH - Runs mce_zero_bias on start
# 2015-12-01 BAS - Triple helium 4 cycle

# --- Valid Names ---
# LS218 Temperatures: ['4K Coldhead', '4K ColdPlate', '55K Coldhead', '55K ColdPlate', '4K Filter', '55K Filter']
# LS370 Temperatures: ['4He Pump', '3He Pump', '4He HX', '3He HX', '4He Cond', '3He Cond', 'IC', 'UC', 'FPU #1', 'FPU #2', 'Spittoon']
# Voltages: ['4He_P', '4He HS', 'IC P', 'IC HS', 'UC P', 'UC HS']



import subprocess
import os
import time

from pyhkfridgelib.fridge_script_base import *
from pyhkdremote.data_loader import pyhkd_get_config_dir

LS370_CONFIG_PATH = os.path.join(pyhkd_get_config_dir(), 'ls370_sk.json')

# True is enabled, False is disabled, None is don't change
LS370_THEMOMETER_ENABLED = [True, True, True, True,
							True, True, True, True,
							None, True, None, True,
							None, None, None, None]

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		# Note: this script can be refactored to re-use the 4He steps!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4,
				self.step5,
				self.step6,
				self.step7,
				self.step8,
				self.step9,
				self.step10,
				self.step11,
				self.step12,
				self.step13,
				self.step14,
				self.step15]
	
	# Initialize everything
	def step0(self):
		
		self.show_status('Enabling LS370 thermometers...')
		
		self.ls370_set_enabled(LS370_CONFIG_PATH, LS370_THEMOMETER_ENABLED)
		
		self.show_status('Attempting to run mce_zero_bias on keck97...')

		subprocess.call(['ssh bicep@keck97.caltech.edu mce_zero_bias &'], shell=True)
			
		self.show_status('Turning off all heat switches and pumps...')
		
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 0.0)
		self.set_voltage('IC P',   0.0)
		self.set_voltage('IC HS',  0.0)
		self.set_voltage('UC P',   0.0)
		self.set_voltage('UC HS',  0.0)
		
		self.show_status('All voltages set to zero.')
		
		self.show_status('Beginning fridge cycle!')
			
		self.move_to_next_step()
	
	# Pause 5 min to let re-enabled thermometers update
	@pause_cycle(duration_seconds = 5*60)
	def step1(self, time_remaining="?"):
		self.show_status('Waiting 5 min for any newly enabled thermometers to update...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)
	
	# Wait for it to settle
	def step2(self):
		
		time_until_settled = (self.SETTLE_TIME - (time.time() - self.last_voltage_update))
		
		if time_until_settled <= 0:
			self.show_status('Temperature is now assumed to have settled')
			self.move_to_next_step()
		else:
			self.show_status('Waiting for temperature to settle (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)

	
	# First He4 heat
	def step2(self):
		self.show_status('Waiting for 4He and 3He heatswitches to cool to 7K...', add_to_log = False)
		if (self.get_temperature('4He HX') < 7) and (self.get_temperature('3He HX') < 7):
			self.show_status('4He and 3He heatswitches have cooled to 7K.')
			self.set_voltage('4He P',  30.0)
			self.show_status('First 4He heat - Set 4He pump to 30V.')
			self.move_to_next_step()
			
	def step3(self):
		self.show_status('Waiting for 4He pump to warm to 55K...', add_to_log = False)
		if self.get_temperature('4He Pump') > 55:
			self.show_status('4He pump has warmed to 55K.')
			self.set_voltage('4He P',  8.0)
			self.show_status('Set 4He pump voltage to quiescent level.')
			self.show_status('Waiting 20 minutes to condense He4.')
			time.sleep(1200)
			self.set_voltage('IC P',  25.0)
			self.set_voltage('UC P',  10.0)
			self.show_status('Set IC and UC pumps to 25V and 10V respectively.')
			self.move_to_next_step()
			
	def step4(self):
		self.show_status('Waiting for 3He pump to warm to 55K...',add_to_log=False)
		if self.get_temperature('3He Pump') > 55:
			self.show_status('3He pump has warmed to 55K.')
			self.set_voltage('IC P',  8.0)
			self.set_voltage('UC P',  7.0)
			self.show_status('Turned down IC and UC pump voltages to quiescent levels')
			self.move_to_next_step()
	
	# First He4 cool
	def step5(self):
		self.show_status('Waiting 5 minutes...',add_to_log=False)
		time.sleep(300)
		self.show_status('Turning off 4He Pump and turning on 4He HS')
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 2.0)
		
		self.move_to_next_step()
			
	def step6(self):
		self.show_status('Waiting for pump to cool to 10K...',add_to_log=False)
		if self.get_temperature('4He Pump') < 10.0:
			self.show_status('4He Pump has cooled to 10K')
			self.set_voltage('4He HS',  0.0)
			self.move_to_next_step()

	# Wait for 4He HX to cool and second He4 heat
	def step7(self):
		self.show_status('Waiting for 4He heatswitch to cool to 7K...',add_to_log=False)
		if (self.get_temperature('4He HX') < 7):
			self.show_status('4He heatswitch has cooled to 7K')
			self.show_status('Turning on 4He P - second 4He heat')
			self.set_voltage('4He P',  30.0)
			self.move_to_next_step()

	# Wait for He4 to heat up
	def step8(self):
		self.show_status('Waiting for 4He pump to warm to 55K...',add_to_log=False)
		if self.get_temperature('4He Pump') > 55:
			self.show_status('4He pump has warmed to 55K, coasting pump.')
			self.set_voltage('4He P',  8.0)
			self.move_to_next_step()

	# Wait with warm pump, then turn on switch
	def step9(self):
		self.show_status('Waiting 30 minutes with warm pump...')
		time.sleep(1800)
		self.show_status('Disabling pump and turning on heat switch')
		self.set_voltage('4He P',0.0)
		self.set_voltage('4He HS',2.0)
		self.move_to_next_step()

	# Wait for pump to cool
	def step10(self):
		self.show_status('Waiting for pump to cool to 10K...',add_to_log=False)
		if self.get_temperature('4He Pump') < 10.0:
			self.show_status('4He Pump has cooled to 10K')
			self.set_voltage('4He HS',  0.0)
			self.move_to_next_step()

	# Wait for 4He HX to cool and third He4 heat
	def step11(self):
		self.show_status('Waiting for 4He heatswitch to cool to 7K...',add_to_log=False)
		if (self.get_temperature('4He HX') < 7):
			self.show_status('4He heatswitch has cooled to 7K')
			self.show_status('Turning on 4He P - third 4He heat')
			self.set_voltage('4He P',  30.0)
			self.move_to_next_step()

	# Wait for He4 to heat up
	def step12(self):
		self.show_status('Waiting for 4He pump to warm to 55K...',add_to_log=False)
		if self.get_temperature('4He Pump') > 55:
			self.show_status('4He pump has warmed to 55K, coasting pump.')
			self.set_voltage('4He P',  8.0)
			self.move_to_next_step()

	# Wait with warm pump, then turn on switch
	def step13(self):
		self.show_status('Waiting 30 minutes with warm pump...')
		time.sleep(1800)
		self.show_status('Disabling pump and turning on heat switch')
		self.set_voltage('4He P',0.0)
		self.set_voltage('4He HS',2.0)
		self.move_to_next_step()
		

	# Wait for pump to cool
	def step14(self):
		self.show_status('Waiting for pump to cool to 10K...',add_to_log=False)
		if self.get_temperature('4He Pump') < 10.0:
			self.show_status('4He Pump has cooled to 10K')
			self.move_to_next_step()
			self.condmin = 10.0
			

	# Last cycle may not consume all He4 right away
	# Wait for He4 to run out to ensure maximum He3 condensation
	# Finally cool He3 pumps
	def step15(self):
		cond = self.get_temperature('IC')
		if cond < self.condmin:
			self.condmin = cond
		if cond > self.condmin + 0.1:
			self.show_status('Condensation point warmed up by 0.1K')
			self.show_status('Turning off IC/UC Pump heaters and turning on IC/UC switches')
			self.set_voltage('IC P',0.0)
			self.set_voltage('UC P',0.0)
			self.set_voltage('IC HS',  3.0)
			self.set_voltage('UC HS',  1.5)
			self.move_to_next_step()

