# Shortkeck Fridge Cycle v0.1.3

# 2014-10-16 JRH - Initial code written based on Animesh/Zak/Jon cycle
# 2015-03-23 JRH - Runs mce_zero_bias on start

# --- Valid Names ---
# LS218 Temperatures: ['4K Coldhead', '4K ColdPlate', '50K Coldhead', '50K ColdPlate', '4K Filter', '50K Filter']
# LS370 Temperatures: ['He4 Pump', 'He3 Pump', 'He4 HX', 'He3 HX', 'He4 Cond', 'He3 Cond', 'IC', 'UC', 'FPU #1', 'FPU #2', 'Spittoon']
# Voltages: ['He4_P', 'He4 HS', 'IC P', 'IC HS', 'UC P', 'UC HS']

from __future__ import division, print_function

import subprocess
from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
        # Note: this script can be refactored to re-use the 4He steps!
		return [self.Step_init,
                self.Step_turnon_heps_1st,
                self.Step_holdon_heps,
                self.Step_pause_warmpump,
				self.Step_turnon_he4hs_1st,
                self.Step_turnon_he3hs,
				self.Step_turnon_he4p_2nd,
                self.Step_holdon_he4p,
                self.Step_pause_warmpump,
                self.Step_turnon_he4hs_2nd]

	
	# Initialize everything
	def Step_init(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 0.0)
		self.set_voltage('3He P',   0.0)
		self.set_voltage('3He HS',  0.0)
		
		self.show_status('All voltages set to zero.')
		
		#self.show_status('Attempting to run mce_zero_bias on keck97...')
		#subprocess.call(['ssh bicep@keck97.caltech.edu mce_zero_bias &'], shell=True)

		self.show_status('Beginning fridge cycle!')
		
		self.move_to_next_step()
	
	# Wait for the heat switches to cool, then turn on the He4 Pump
	def Step_turnon_heps_1st(self):
		
		self.show_status('Waiting for He4 and He3 heatswitches to cool to 7K...', add_to_log = False)
		
		if (self.get_temperature('4He HS') < 7) and (self.get_temperature('3He HS') < 7):
			
			self.show_status('He4 and He3 heatswitches to have cooled to 7K.')
			
			self.set_voltage('4He P',  20.0)
			self.set_voltage('3He P',  20.0)
			self.show_status('Set He4 & He3 Pump to 20V.')
			
			self.move_to_next_step()
			
	# Wait for the He4 and He3 Pump to warm up, then hold for 40mins.
	def Step_holdon_heps(self):
		
		self.show_status('Waiting for He3 Pump to warm to 40K...', add_to_log = False)
		
		if self.get_temperature('3He Pump') > 40:
			
			self.show_status('He3 Pump has warmed to 40K. ')
			self.set_voltage('3He P',  5.5)
			self.show_status('Set He3 Pump voltage (5.5) to quiescent level.')
			
			self.show_status('Waiting for He4 Pump to warm to 50K...', add_to_log = False)
			
			if self.get_temperature('4He Pump') > 50:
				self.show_status('He4 Pump has warmed to 50K. ')
				self.set_voltage('4He P',  7.5)
				self.show_status('Set He4 Pump voltage (7.5) to quiescent level.')
			
				self.move_to_next_step()

	@pause_cycle(duration_seconds = 40*60)
	def Step_pause_warmpump(self, time_remaining="?"):
		self.show_status('Waiting 40 minutes with warm pump...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)		

	# Cool down He4 Pump and switch on He4 HS for first time. 
	def Step_turnon_he4hs_1st(self):
		
		self.show_status('Turn off He4 Pump. He4 heat switch set to 1.0V.')
		self.set_voltage('4He P',  0)
		self.set_voltage('4He HS', 1.0)
			
		self.move_to_next_step()
			
		
	def Step_turnon_he3hs(self):
		
		self.show_status('Waiting for He4 pump to cool to 5.5K...', add_to_log = False)
		
		if self.get_temperature('4He Pump') < 5.5:
			
			self.show_status('He4 pump has cooled to 5.5K.')
			
			self.show_status('Turn off He3 Pump. He3 heat switch set to 1.0V.')
			self.set_voltage('3He P',  0)
			self.set_voltage('3He HS', 1.0)
			
			self.move_to_next_step()
			
	def Step_turnon_he4p_2nd(self):
		
		self.show_status('Waiting for He4 Pump to cool to 5K...', add_to_log = False)
		
		if self.get_temperature('4He Pump') < 5.0:
			
			self.show_status('He4 Pump has cooled to 5.0K.')
			self.show_status('Turning on He4 pump (20.0) - second He4 heat. He4 heat switch set to 0.0V.')
			self.set_voltage('4He P', 20.0)
			self.set_voltage('4He HS', 0.0)

			self.move_to_next_step()
			
	def Step_holdon_he4p(self):
		
		self.show_status('Waiting for He4 pump to warm to 50 K...', add_to_log = False)
		
		if self.get_temperature('4He Pump') > 50.0:
			self.show_status('He4 pump has heated to 50.0K.')
			self.set_voltage('4He P',  7.5)
			self.show_status('Set He4 Pump voltage (7.5V) to quiescent level.')
			
			self.move_to_next_step()
	
	@pause_cycle(duration_seconds = 70*60)
	def Step_pause_warmpump(self, time_remaining="?"):
		self.show_status('Waiting 40 minutes with warm pump...  (' + str(int(time_remaining)) + ' sec remaining)', add_to_log=False)		
			
	# Wait for the IC Evap to reach 2.2K, then turn on the He3 heatswitch
	def Step_turnon_he4hs_2nd(self):
		
		self.show_status('Turn off He4 Pump. He4 heat switch set to 1.0V. Done!')
		self.set_voltage('4He P',  0)
		self.set_voltage('4He HS', 1.0)
			
		self.move_to_next_step()
