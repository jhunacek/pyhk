# K0 Cooldown Helper Script v1.0.2
#  Copied from SK cooldown

# 2014-11-15 JRH - Initial code

# --- Valid Names ---
# LS218 Temperatures: ['4K Coldhead', '4K ColdPlate', '50K Coldhead', '50K ColdPlate', '4K Filter', '50K Filter']
# LS370 Temperatures: ['4He Pump', '3He Pump', '4He HX', '3He HX', '4He Cond', '3He Cond', 'IC', 'UC', 'FPU #1', 'FPU #2', 'Spittoon']
# Voltages: ['4He_P', '4He HS', 'IC P', 'IC HS', 'UC P', 'UC HS']

from __future__ import division, print_function

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		return [self.step_init, self.step_wait]
	
	# Initialize everything
	def step_init(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.set_voltage('He4 Pump',  0.0)
		self.set_voltage('He4 HS', 0.0)
		self.set_voltage('He3 Pump',   0.0)
		self.set_voltage('He3 HS',  0.0)
		
		self.show_status('All voltages set to zero.')
		
		self.move_to_next_step()
	
	# Wait for the heat switches to cool, then turn on the pumps
	def step_wait(self):
		
		self.show_status('Waiting for 4He and 3He heatswitches to cool to 12K...', add_to_log = False)
		
		if (self.get_temperature('Fridge: He4 HS') < 12) and (self.get_temperature('Fridge: He3 HS') < 12):
			
			self.show_status('He4 and He3 heatswitches have cooled to 12K.')
			
			self.set_voltage('He4 Pump',  8.)
			self.show_status('Set He4 Pump to 8V.')

			self.set_voltage('He3 Pump',  7.)
			self.show_status('Set He3 Pump to 7V.')
			
			self.move_to_next_step()
		
