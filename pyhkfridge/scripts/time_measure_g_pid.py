# TIME G Measurement v1.0

# 2015-06-05 JRH - Initial code
# 2016-02-08 JRH - Modified to use ivcurve.py
# 2017-11-08 JRH - Modified for TIME cryostat
# 2021-09-21 JRH - Modified to use the FPU temperature PID, which should settle faster

import datetime
import numpy as np
import time
import subprocess

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	UNLATCH_FIRST = False # Set to False only if you are starting unlatched and settled already
	EXTEND_EARLY = True # Extend the wait for the first two points by 1.5x 
	UNLATCH_HEATER_NAME = "FPU Heater #2"
	HEATER_NAME = "FPU Heater #2"
	THERMOMETER_NAME = "Spectrometer A"
	AUX_THERMOMETER_NAMES = ["FPU #1","FPU #2","Fridge UC","Fridge IC","Spectrometer A","Module A0","Module A1","Module A5"] # Other thermometers to save
	AUX_RESISTOR_NAMES = [] # Other resistances to save
	TEMPERATURE_VALUES = [0.350, 0.375, 0.400, 0.425, 0.450, 0.475, 0.500]
	# ~ TEMPERATURE_VALUES = [0.300, 0.325, 0.350, 0.375, 0.400, 0.425, 0.450, 0.475, 0.500, 0.550, 0.600, 0.650, 0.700]
	# ~ TEMPERATURE_VALUES = [0.312, 0.337, 0.362, 0.387, 0.412, 0.437, 0.462, 0.487, 0.525, 0.575, 0.625, 0.675, 0.725]
	SETTLE_TIME = 40*60 # Seconds (time to wait after changing a power)
	# ~ REMOTE_WAIT_TIME = 400 # Seconds (time to wait for remote commands to finish) (should be at LEAST bias_pause * bias_count!!!!!)
	# ~ REMOTE_WAIT_TIME = 200 # Seconds (time to wait for remote commands to finish) (should be at LEAST bias_pause * bias_count!!!!!)
	REMOTE_WAIT_TIME = 500 # Seconds (time to wait for remote commands to finish) (should be at LEAST bias_pause * bias_count!!!!!)
	# ~ UNLATCH_CMD = 'bias_tess 30000 && sleep 20 && bias_tess 2000'
	# ~ UNLATCH_CMD = 'bias_tess 30000 && sleep 20 && bias_tess 3500'
	UNLATCH_CMD = 'bias_tess 30000 && sleep 20 && bias_tess 3000'
	# ~ IVCURVE_CMD = '/home/time/b3_analysis/load_curve/ivcurve.py --zap_bias 3000 --zap_time 1 --bias_start 2000 --bias_step -1 --bias_count 2001 --settle_time 30 --settle_bias 2000 --bias_pause 0.1 --bias_final 2000 --data_mode 10 -d '
	# ~ IVCURVE_CMD = '/home/time/b3_analysis/load_curve/ivcurve.py --zap_bias 3000 --zap_time 1 --bias_start 3500 --bias_step -1 --bias_count 3501 --settle_time 30 --settle_bias 3500 --bias_pause 0.03 --bias_final 0 --data_mode 10 -d '
	IVCURVE_CMD = '/home/time/b3_analysis/load_curve/ivcurve.py --zap_bias 3000 --zap_time 1 --bias_start 3000 --bias_step -1 --bias_count 3001 --settle_time 30 --settle_bias 3000 --bias_pause 0.1 --bias_final 0 --data_mode 10 -d '
	BLAST_POWER = 300*1e-6 # W
	BLAST_TIME = 40 # sec	

	# NOTE: bias the TESs to the settle bias before starting or the first temperature will latch!
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4,
				self.step5,
				self.step6,
				]
				
	# Initialize this run
	def step0(self):
		
		self.show_status('Starting G measurment, monitoring temperature ' + self.THERMOMETER_NAME)
		self.show_status('NOTE: Are the SQUIDs tuned?')
		
		timestamp = int(time.time())
		self.savefile_name = '/home/time/glog/time_g_' + str(timestamp) + '.txt'
		self.loadcurve_prefix = 'iv_g_' + str(timestamp) 
				
		with open(self.savefile_name, 'a') as savefile:
			savefile.write('--- TIME G Measurement ---\n\n')
			savefile.write('Thermometer: ' + str(self.THERMOMETER_NAME) + '\n')
			savefile.write('Heater: ' + str(self.HEATER_NAME) + '\n')
			savefile.write('Starting Time: ' + str(datetime.datetime.now()) + '\n')
			savefile.write('Settle Time (sec): ' + str(self.SETTLE_TIME) + '\n')
			savefile.write('\nTime [s]\Temperature Applied [K]\Power Measured [uW]\tTemperature Measured [K]')
			for n in self.AUX_THERMOMETER_NAMES:
				savefile.write('\t' + str(n) + ' [K]')
			for n in self.AUX_RESISTOR_NAMES:
				savefile.write('\t' + str(n) + ' [Ohm]')
			savefile.write('\n')
		
		self.current_t_index = 0
		
		self.show_status('Planning to try the following temperatures in K: ' + str(self.TEMPERATURE_VALUES))
		
		if self.UNLATCH_FIRST:
			# Start in a known state
			self.output_off(self.HEATER_NAME)
			self.output_off(self.UNLATCH_HEATER_NAME)
		
			self.move_to_next_step()
		else:
			self.show_status('Skipping setup for first point')
			self.move_to_step(3)

	# Blast normal and set the new target power
	def step1(self):
		
		new_temperature =  self.TEMPERATURE_VALUES[self.current_t_index]
		
		self.show_status('Blasting TES bias to unlatch...')
		subprocess.call(['ssh time@time-mce-0.caltech.edu "' + self.UNLATCH_CMD + '" &'], shell=True)

		self.show_status('Blasting unlatch heater with ' + str(self.BLAST_POWER*1e6) + ' uW for ' + str(self.BLAST_TIME) + ' sec to unlatch...')
		self.set_power(self.UNLATCH_HEATER_NAME, self.BLAST_POWER)
		time.sleep(self.BLAST_TIME)
		self.set_power(self.UNLATCH_HEATER_NAME, 0)
		self.show_status('Setting Temperature: ' + str(new_temperature))
		self.start_temperature_regulation(self.HEATER_NAME, new_temperature)
		self.last_power_update = time.time()
		
		self.local_settle_time = self.SETTLE_TIME
		
		# Give the low power points a little extra time, since it
		# tends to need it
		if self.EXTEND_EARLY and self.current_t_index <= 1:
			self.local_settle_time *= 1.5
		
		self.move_to_next_step()
	
	# Wait for it to settle
	def step2(self):
		
		time_until_settled = (self.local_settle_time - (time.time() - self.last_power_update))
		
		if time_until_settled <= 0:
			self.show_status('Temperature is now assumed to have settled')
			self.move_to_next_step()
		else:
			self.show_status('Waiting for temperature to settle (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
			
	# Save the temperatures, take load curve
	def step3(self):
		
		temperature_applied = round(self.TEMPERATURE_VALUES[self.current_t_index],3)
		power_measured = round(self.get_power(self.HEATER_NAME)*1e6,3)
		temperature_measured = round(self.get_temperature(self.THERMOMETER_NAME),4)
		
		self.show_status('Results for ' + str(temperature_applied) + 'K applied: ' + str(power_measured) + ' uW, ' + str(temperature_measured) + ' K')

		with open(self.savefile_name, 'a') as savefile:
			savefile.write(str(time.time()) + '\t' + str(temperature_applied) + '\t' + str(power_measured) + '\t' + str(temperature_measured) )
			for n in self.AUX_THERMOMETER_NAMES:
				savefile.write('\t' + str(round(self.get_temperature(n),4)))
			for n in self.AUX_RESISTOR_NAMES:
				savefile.write('\t' + str(round(self.get_resistance(n),4)))
			savefile.write('\n')
			
		# Take the load curve on time-mce-0
		try:
			lc_filename = self.loadcurve_prefix + '_' + str(int(round(1000*temperature_measured))) + 'mk'
		except ValueError:
			lc_filename = self.loadcurve_prefix + '_step' + str(self.current_t_index)
		self.show_status('Attempting to begin load curve ' + lc_filename + ' on time-mce-0.')
		subprocess.call(['ssh time@time-mce-0.caltech.edu ' + self.IVCURVE_CMD + lc_filename + ' &'], shell=True)
		self.loadcurve_start_time = time.time()
		
		self.move_to_next_step()
	
	# Wait for the load curve to finish before changing the power
	def step4(self):
		
		time_until_settled = (self.REMOTE_WAIT_TIME - (time.time() - self.loadcurve_start_time))
		
		if time_until_settled > 0:
			self.show_status('Pausing to allow the load curve to finish (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
		else:	
			self.show_status('Load curve is assumed to have finished.')
			self.move_to_next_step()

	def step5(self):
		
		# Decide if we are finished or not
		self.current_t_index += 1
		if self.current_t_index >= len(self.TEMPERATURE_VALUES):
			self.move_to_next_step()
		else:
			self.move_to_step(1)
	
	# Write out any final state data
	def step6(self):
		
		self.show_status('Turning heater off.')
		self.output_off(self.HEATER_NAME)
		
		with open(self.savefile_name, 'a') as savefile:
			savefile.write('\n\n')
			savefile.write('Ending Time: ' + str(datetime.datetime.now()) + '\n')
			savefile.write('\n\n-------------------\n\n')
			
		self.show_status('Measurement complete!')
			
		self.move_to_next_step()
	

