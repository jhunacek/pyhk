# Shortkeck Cooldown Helper Script v1.0.2

# 2014-11-15 JRH - Initial code

# --- Valid Names ---
# LS218 Temperatures: ['4K Coldhead', '4K ColdPlate', '50K Coldhead', '50K ColdPlate', '4K Filter', '50K Filter']
# LS370 Temperatures: ['4He Pump', '3He Pump', '4He HX', '3He HX', '4He Cond', '3He Cond', 'IC', 'UC', 'FPU #1', 'FPU #2', 'Spittoon']
# Voltages: ['4He_P', '4He HS', 'IC P', 'IC HS', 'UC P', 'UC HS']



from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		return [self.step_init, self.step_wait]
	
	# Initialize everything
	def step_init(self):
		
		self.show_status('Turning off all heat switches and pumps...')
		
		self.set_voltage('4He P',  0.0)
		self.set_voltage('4He HS', 0.0)
		self.set_voltage('IC P',   0.0)
		self.set_voltage('IC HS',  0.0)
		self.set_voltage('UC P',   0.0)
		self.set_voltage('UC HS',  0.0)
		
		self.show_status('All voltages set to zero.')
		
		self.move_to_next_step()
	
	# Wait for the heat switches to cool, then turn on the pumps
	def step_wait(self):
		
		self.show_status('Waiting for 4He and 3He heatswitches to cool to 10K...', add_to_log = False)
		
		if (self.get_temperature('4He HX') < 15) and (self.get_temperature('3He HX') < 15):
			
			self.show_status('4He and 3He heatswitches have cooled to 10K.')
			
			self.set_voltage('4He P',  8.)
			self.show_status('Set 4He pump to 8V.')

			self.set_voltage('IC P',  8.)
			self.show_status('Set IC pump to 8V.')

			self.set_voltage('UC P',  7.)
			self.show_status('Set UC pump to 7V.')
			
			self.move_to_next_step()
		
