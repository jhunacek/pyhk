# LE Cryostat Fridge Cycle v1.0

# 2016-04-05 JRH - Initial code



import time
from pyhkfridgelib.fridge_script_base import *

WAIT_TIME = 20#2*60*60 # sec
CURRENT_TARGET = 9 # A
RAMP_RATE = 0.01 # A/sec

ADR_MODE_PAUSE = 0
ADR_MODE_RAMP = 1
ADR_MODE_ZERO = 2

class FridgeScript(FridgeScriptBase):
	
	def get_steps(self):
		return [self.step0, self.step1, self.step2]
	
	# Initialize everything
	def step0(self):
		
		self.show_status('Ramping to ' + str(CURRENT_TARGET) + ' A at ' + str(RAMP_RATE) + ' A/sec')
		
		self.set_current('ADR', CURRENT_TARGET)
		self.set_currentramp('ADR', RAMP_RATE)
		self.set_devicestate('ADR', ADR_MODE_RAMP)
		
		self.start_time = time.time()

		self.move_to_next_step()
		
	# Wait
	def step1(self):
		
		time_until_settled = (WAIT_TIME - (time.time() - self.start_time))
		
		if time_until_settled <= 0:
			self.move_to_next_step()
		else:
			self.show_status('Waiting for magnet to ramp and soak (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
	
	# Ramp down
	def step2(self):
		
		self.show_status('Ramping down the current', add_to_log = False)
		self.set_devicestate('ADR', ADR_MODE_ZERO)
		self.move_to_next_step()
			

