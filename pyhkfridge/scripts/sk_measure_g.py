# ShortKeck G Measurement v1.0

# 2015-06-05 JRH - Initial code
# 2016-02-08 JRH - Modified to use ivcurve.py



import datetime
import numpy as np
import time
import subprocess

from pyhkfridgelib.fridge_script_base import *

class FridgeScript(FridgeScriptBase):
	
	HEATER_NAME = "FPU Heater"
	THERMOMETER_NAME = "FPU #1"
	AUX_THERMOMETER_NAMES = [] # Other thermometers to save
	AUX_RESISTOR_NAMES = ["FPU #2"] # Other resistances to save
	MAX_VOLTAGE = 2.1 # V
	SETTLE_TIME = 45*60 # Seconds (time to wait after changing a voltage)
	REMOTE_WAIT_TIME = 60 # Seconds (time to wait for remote commands to finish)
	NUM_VOLTAGE_STEPS = 7
	IVCURVE_CMD = '/home/bicep/b3_analysis/load_curve/ivcurve.py -c 1 2 3 4 5 6 9 10 12 13 14 --bias_start 16000 --bias_step -20 --bias_count 801 --settle_time 0 --settle_bias 5000 --bias_pause 0.01 --bias_final 5000 --data_mode 10 -d '
	BLAST_VOLTAGE = 5 # V
	BLAST_TIME = 20 # sec
	
	def get_steps(self):
		# Note: functions should be renamed something more descriptive!
		return [self.step0,
				self.step1,
				self.step2,
				self.step3,
				self.step4,
				self.step5]
				
	# Initialize this run
	def step0(self):

		self.show_status('Starting G measurment, monitoring temperature ' + self.THERMOMETER_NAME)
		
		timestamp = int(time.time())
		self.savefile_name = './shortkeck_g_' + str(timestamp) + '.txt'
		self.loadcurve_prefix = 'iv_g_' + str(timestamp) 
		
		# Start in a known state
		self.output_off(self.HEATER_NAME)
		
		with open(self.savefile_name, 'a') as savefile:
			savefile.write('--- ShortKeck G Measurement ---\n\n')
			savefile.write('Thermometer: ' + str(self.THERMOMETER_NAME) + '\n')
			savefile.write('Heater: ' + str(self.HEATER_NAME) + '\n')
			savefile.write('Starting Time: ' + str(datetime.datetime.now()) + '\n')
			savefile.write('Settle Time (sec): ' + str(self.SETTLE_TIME) + '\n')
			savefile.write('\nTime [s]\tVoltage Applied [V]\tVoltage Measured [V]\tTemperature Measured [K]')
			for n in self.AUX_THERMOMETER_NAMES:
				savefile.write('\t' + str(n) + ' [K]')
			for n in self.AUX_RESISTOR_NAMES:
				savefile.write('\t' + str(n) + ' [Ohm]')
			savefile.write('\n')
		
		# Linearly sample power space
		self.voltages_to_use = np.round(np.sqrt(np.linspace(0,self.MAX_VOLTAGE**2,self.NUM_VOLTAGE_STEPS)),2).tolist()
		
		## Linearly sample voltage space
		#self.voltages_to_use = np.round(np.linspace(0,self.MAX_VOLTAGE,self.NUM_VOLTAGE_STEPS),2).tolist()
		
		self.current_voltage_index = 0
		
		self.show_status('Planning to try the following voltages in V: ' + str(self.voltages_to_use))
		
		self.move_to_next_step()

	# Blast normal and set the new target voltage
	def step1(self):
		
		new_voltage =  self.voltages_to_use[self.current_voltage_index]
		
		self.show_status('Blasting with ' + str(self.BLAST_VOLTAGE) + ' V for ' + str(self.BLAST_TIME) + ' sec')
		self.set_voltage(self.HEATER_NAME, self.BLAST_VOLTAGE)
		time.sleep(self.BLAST_TIME)
		self.show_status('Setting Voltage: ' + str(new_voltage))
		self.set_voltage(self.HEATER_NAME, new_voltage)
		self.last_voltage_update = time.time()
		
		self.move_to_next_step()
	
	# Wait for it to settle
	def step2(self):
		
		time_until_settled = (self.SETTLE_TIME - (time.time() - self.last_voltage_update))
		
		if time_until_settled <= 0:
			self.show_status('Temperature is now assumed to have settled')
			self.move_to_next_step()
		else:
			self.show_status('Waiting for temperature to settle (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
			
	# Save the temperatures, take load curve
	def step3(self):
		
		voltage_applied = round(self.voltages_to_use[self.current_voltage_index],3)
		voltage_measured = round(self.get_voltage(self.HEATER_NAME),3)
		temperature_measured = round(self.get_temperature(self.THERMOMETER_NAME),3)
		
		self.show_status('Results for ' + str(voltage_applied) + ' V applied: ' + str(voltage_measured) + ' V, ' + str(temperature_measured) + ' K')

		with open(self.savefile_name, 'a') as savefile:
			savefile.write(str(time.time()) + '\t' + str(voltage_applied) + '\t' + str(voltage_measured) + '\t' + str(temperature_measured) )
			for n in self.AUX_THERMOMETER_NAMES:
				savefile.write('\t' + str(round(self.get_temperature(n),3)))
			for n in self.AUX_RESISTOR_NAMES:
				savefile.write('\t' + str(round(self.get_resistance(n),3)))
			savefile.write('\n')
			
		# Take the load curve on keck97
		try:
			lc_filename = self.loadcurve_prefix + '_' + str(int(round(1000*temperature_measured))) + 'mk'
		except ValueError:
			lc_filename = self.loadcurve_prefix + '_step' + str(self.current_voltage_index)
		self.show_status('Attempting to begin load curve ' + lc_filename + ' on keck97.')
		#subprocess.call(['ssh bicep@keck97.caltech.edu ramp_tes_bias ' + lc_filename + ' s &'], shell=True)
		subprocess.call(['ssh bicep@keck97.caltech.edu ' + self.IVCURVE_CMD + lc_filename + ' &'], shell=True)
		self.loadcurve_start_time = time.time()
		
		self.move_to_next_step()
	
	# Wait for the load curve to finish before changing the voltage
	def step4(self):
		
		time_until_settled = (self.REMOTE_WAIT_TIME - (time.time() - self.loadcurve_start_time))
		
		if time_until_settled > 0:
			self.show_status('Pausing to allow the load curve to finish (' + str(int(time_until_settled)) + ' sec remaining)', add_to_log=False)
		else:	
			self.show_status('Load curve is assumed to have finished.')
			
			# Decide if we are finished or not
			self.current_voltage_index += 1
			if self.current_voltage_index >= self.NUM_VOLTAGE_STEPS:
				self.move_to_next_step()
			else:
				self.move_to_step(1)
	
	# Write out any final state data
	def step5(self):
		
		self.show_status('Turning heater off.')
		self.output_off(self.HEATER_NAME)
		
		with open(self.savefile_name, 'a') as savefile:
			savefile.write('\n\n')
			savefile.write('Ending Time: ' + str(datetime.datetime.now()) + '\n')
			savefile.write('\n\n-------------------\n\n')
			
		self.show_status('Measurement complete!')
			
		self.move_to_next_step()
	

