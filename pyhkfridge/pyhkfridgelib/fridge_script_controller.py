'''
Manages and runs a fridge cycle
'''



import logging
import datetime
import importlib
import traceback
import threading
import os
import sys
import time
import socket
import urllib.request, urllib.parse, urllib.error

from pyhkfridgelib.fridge_script_base import FridgeScriptBase
from pyhkfridgelib.settings import PYHKFRIDGE_IP, FRIDGE_STATE_BASE_FOLDER, FRIDGE_LOG_BASE_FOLDER
from pyhkfridgeremote.data_loader import pyhkfridge_get_log_filename, pyhkfridge_get_state_folder
from packetcomm.packetcomm import PacketServer
import imp

class FridgeScriptController(PacketServer):
	
	# If default_script is a file path, load it as the default fridge script
	def __init__(self, port, default_script = None):
		
		self._running = False
		self._current_step = 0
		self._str_last_runstate = ''
		self.step_start_time = 0
		
		self._script_status_lock = threading.Lock()	
		self._step_lock = threading.Lock()
		self._logging_lock = threading.Lock()
			
		self._port = port
				
		self._dirname_state = pyhkfridge_get_state_folder(FRIDGE_STATE_BASE_FOLDER, self._port)
		if not os.path.exists(self._dirname_state ):
			os.makedirs(self._dirname_state )
			
		self.autorun_disable()
		self.set_autorun_time(2,0)
		self.update_runstate_str()	
		
		self.fridgescript = FridgeScriptBase(self)
		
		if default_script != None:
			self._load_script(default_script)
		else:
			self.write_fridge_state('script_file', '')
			self.write_fridge_state('text_script', '')
			self.write_fridge_state('text_conf', '')
			
		# Bound to localhost so external commands are not accepted
		PacketServer.__init__(self, "localhost", self._port)
		
		self.set_status("Initialized")


	# Main loop of the fridge script controller.  Never returns
	def main_loop(self):
		
		logging.info("Fridge script controller main loop starting")
		
		while True:
			
			# Don't waste CPU time for something this long-term
			time.sleep(1)

			# Run the current cycle if it is running
			self._execute_steps()
			
			# Autorun if it is time to
			if datetime.datetime.now() > self._next_autorun_datetime:
				self._on_autorun_time()
				self._schedule_next_autorun()
				
		logging.info("Fridge script controller main loop dying")
		
	def handle_packet(self, data):

		try:
			command_split = data.rstrip().split(",")
			
			# Make sure we have the right number
			if len(command_split) < 1:
				raise ValueError("Empty command")
				
			command = command_split[0]
			
		except (TypeError, AttributeError, ValueError):
			logging.error("Invalid packet: " + str(data))	
			return
			
		# Handle the command
		logging.info("Received command: " + str(data))	
		if command == 'load':
			if len(command_split) > 1:
				filename = urllib.parse.unquote(command_split[1])
				if os.path.isfile(filename):
					logging.info("Attempting to load file: " + str(filename))
					self._load_script(filename)
				else:
					logging.error("Asked to load non-existing file: " + str(filename))
			else:
				logging.error("Asked to load file, but no file name provided")
		elif command == 'reload_conf':
			self._config_file_reload()
		elif command == 'start':
			self.start_script()
		elif command == 'stop':
			self.stop_script()
		elif command == 'skip':
			self.move_to_next_step()
		elif command == 'auto_on':
			self.autorun_enable()
		elif command == 'auto_off':
			self.autorun_disable()
		elif command == 'auto_time':
			if len(command_split) >= 3:
				try:
					hour = int(command_split[1])
					minute = int(command_split[2])
					self.set_autorun_time(hour, minute)
				except ValueError:
					logging.error("Asked to change autorun time, but invalid time provided")
			else:
				logging.error("Asked to change autorun time, but time not provided")
		else:
			logging.error("Unable to handle command")		
	
	# Figure out the next time we need to autorun the fridge
	def _schedule_next_autorun(self):
		
		today = datetime.date.today()
		self._next_autorun_datetime = datetime.datetime.combine(today, self._autorun_time)
		
		if self._next_autorun_datetime < datetime.datetime.now():
			self._next_autorun_datetime += datetime.timedelta(days=1)
			
			
	##########################
	##### Script Control #####
	##########################
	
	# Move to the next script step	
	def move_to_next_step(self):
		with self._script_status_lock:
			next_step = self._current_step + 1
		self.move_to_step(next_step)
		
	# Move to a script step.  If the step is above the valid range, complete
	# the fridge script.  If it is negative or not an integer, fail the script.
	def move_to_step(self, new_step):

		try:
			new_step = int(new_step)
			if new_step < 0:
				raise ValueError()
		except:
			self.fail_script("Cannot move to invalid step " + str(new_step))
		else:
					
			with self._script_status_lock:
				
				self.step_start_time = time.time()

				if self._running:
					
					# Check if we finished
					if new_step >= len(self.fridgescript.steps):
						self._running = False
						self._current_step = 0
						self.set_status("Fridge Script Completed")
					else:
						self._current_step = new_step
						fname = self.fridgescript.steps[self._current_step].__name__
						self.set_status("Moving to step number %i (function: %s)" % (self._current_step, fname))
		
		self.update_runstate_str()			
		
	# Start the fridge script immediately from step "start_step_id"
	def start_script(self, start_step_id = 0):
		
		with self._script_status_lock:
			self._running = True
			
		if start_step_id != 0:
			self.set_status("Fridge Script Starting at Step ID " + str(start_step_id))
		else:
			self.set_status("Fridge Script Starting")
		
		self.move_to_step(start_step_id)
	
	# Stops the script and returns the ID of the current step it was on
	def stop_script(self):
		
		with self._script_status_lock:
			
			if self._running:
				self.set_status("Stopping a Running Fridge Script (At Step " + str(int(self._current_step)) + ")")
			
			self._running = False
			stop_step_id = self._current_step
			self._current_step = 0		
			
		self.update_runstate_str()	
					
		return stop_step_id
		
	# Stops the script and returns the ID of the current step it was on.  Failure notifications
	# are sent out if configured.
	def fail_script(self, reason='Unspecified'):
		
		with self._script_status_lock:
			
			if self._running:
				self.set_status("Fridge Script Failed At Step " + str(int(self._current_step)) + ".  Reason: " + str(reason))
			
			self._running = False
			stop_step_id = self._current_step
			self._current_step = 0		
		
		self.update_runstate_str()	
		
		## TODO: send failure notification
			
		return stop_step_id
		
	# Stops a script after completeting successfully
	def complete_script(self):
		
		with self._script_status_lock:
			
			if self._running:
				self.set_status("Fridge Script Completed Successfully")
			
			self._running = False
			self._current_step = 0		
			
		self.update_runstate_str()	
			
	def autorun_enable(self):
		with self._script_status_lock:
			logging.info("Enabling autorun")
			self._autorun_enabled = True
			self.write_fridge_state('autorun_enabled', 'True')	
	
	def autorun_disable(self):
		with self._script_status_lock:
			logging.info("Disabling autorun")
			self._autorun_enabled = False
			self.write_fridge_state('autorun_enabled', 'False')	
			
	def set_autorun_time(self, hour, minute):
		
		try:
			hour = int(hour)
			minute = int(minute)
			
			if (hour < 0) or (hour > 23) or (minute < 0) or (minute > 59):
				raise ValueError("Invalid time")
				
		except ValueError:
			logging.error("Can't set autorun time to invalid time: " + str(hour) + " " + str(minute))
			return_code
					
		with self._script_status_lock:
			logging.info("Changing autorun time")
			self._autorun_time = datetime.time(hour,minute)
			self._schedule_next_autorun()
			self.write_fridge_state('autorun_time', str(hour) + ',' + str(minute))	
					
	# Runs at the scheduled autorun time.  If the fridge script is 
	# supposed to run, start it.
	def _on_autorun_time(self):
		
		self.set_status("Auto fridge script time reached.")
		
		# We can't hold this lock and call start_script
		with self._script_status_lock:
			is_running = self._running
			autorun_enabled = self._autorun_enabled
			
		if not is_running:
			if autorun_enabled:
				self.start_script()
			else:
				self.set_status("Fridge script auto-run is disabled, taking no action.")
		else:
			self.set_status("Script is already running, fridge script auto-run is taking no action.")
		
	# Run the next steps of the fridge script
	def _execute_steps(self):
		
		# Don't ever run two steps at once
		with self._step_lock:
				
			with self._script_status_lock:
				
				# We can't keep this lock, so cache the result
				running = self._running
				current_step = self._current_step
						
			#  Check if we are running
			if (running) and (current_step < len(self.fridgescript.steps)) and (current_step >= 0):
				
				# Call the current script step
				success = self.fridgescript.execute_step(current_step)
				
				if not success:
					self.stop_script()
					
			# Check if we finished
			with self._script_status_lock:
				running = self._running
				current_step = self._current_step
			if (running) and (current_step >= len(self.fridgescript.steps)) and (len(self.fridgescript.steps) > 0):
				self.complete_script()
					
	############################
	##### Status Reporting #####
	############################
		
	# Add something to the fridge script log.
	def log(self, text):
		with self._logging_lock:
			to_show =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " - " + str(text)
			logging.info("Frige Log: " + to_show)
			
			# Write to a log file
			fn = pyhkfridge_get_log_filename(FRIDGE_LOG_BASE_FOLDER, datetime.datetime.today(), self._port)

			dirname = os.path.dirname(fn)
			try: 
				os.makedirs(dirname)
			except OSError:
				if not os.path.isdir(dirname):
					raise
			with open(fn, 'a') as f:
				f.write(to_show + '\n')
				
					
	# Show "text" as the current status.  If log == True, also add it to the log
	def set_status(self, text, add_to_log = True):
		
		with self._logging_lock:
			
			to_set = str(text) + " <i>(" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ")</i>"
			self.write_fridge_state('status', to_set)
			
		if add_to_log:
			self.log(text)	
			
	# Helper function to automatically update the Running/Not Running label.
	# Run this whenever you think the contents of this indicator may have changed.
	def update_runstate_str(self):
		
		with self._script_status_lock:
				
			if self._running:
				fname = self.fridgescript.steps[self._current_step].__name__
				to_set = '<font color=\"green\">Running step %i (function: %s)</font>' % (self._current_step, fname)
			else:
				to_set = 'Not Running'
			
			if to_set != self._str_last_runstate:
				self.write_fridge_state('runstate', to_set)
				self._str_last_runstate = to_set
				
	# Write to temporary state files read by other programs
	def write_fridge_state(self, varname, value):			
		with open(os.path.join(self._dirname_state ,urllib.parse.quote(varname)), 'w') as f:
			f.write(str(value))
		
	##########################
	##### Script Loading #####
	##########################

	# Load a new set of script steps from a file
	def _load_script(self, file_path):
		
		self.stop_script()
			
		# Don't change the steps while one is running, that would be pretty awful
		with self._step_lock:
		
			try:
				file_path_abs = os.path.abspath(str(file_path))
				file_path_rel = os.path.relpath(str(file_path))
				module_name = file_path_rel.replace('./','').replace('.py','').replace('/','.').replace('\\','.')
			
				i = importlib.import_module(module_name) # Import it in case we never have before
				i = imp.reload(i) # Make sure to force it to reload from disk in case it was already imported
				file_dir = os.path.dirname(os.path.realpath(i.__file__))
				self.fridgescript = i.FridgeScript(self, file_dir)
		
				self.write_fridge_state('script_file', file_path_abs)
				
				with open(file_path_abs, 'r') as f:
					text_script = f.read()
				self.write_fridge_state('text_script', text_script)
								
				self.set_status("Loaded new fridge script steps from file (" + file_path_rel + ", " + str(len(self.fridgescript.steps)) + " steps)")
				
			except Exception:
				self.set_status("Fridge script steps could not be loaded from file!")
				self.log('Contained Fridge Script File Load Error:\n' + str(traceback.format_exc()).rstrip())
				self.fridgescript = FridgeScriptBase(self)
				self.write_fridge_state('script_file', '')
			

	# Tell the fridge cycle to reload the config file from disk
	def _config_file_reload(self):
		
		logging.info("Reloading config file...")
		
		# Don't change the config while a step is running
		with self._step_lock:
			try:
				self.fridgescript.config_reload()
			except Exception as e:
				logging.warning("Unable to reload config file.  " + str(e))

			
