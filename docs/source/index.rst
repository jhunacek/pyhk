.. PyHK documentation master file, created by
   sphinx-quickstart on Sun Aug  2 14:12:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyHK v\ |release|
########################################################################

A suite of Python software developed to collect, store, and view cryogenic
housekeeping data and to control heaters and fridges.

.. toctree::
   :maxdepth: 3
   
   overview
   installation
   configuration
   extension
   troubleshooting
   faq


.. ~ Indices and tables
.. ~ ==================

.. ~ * :ref:`genindex`
.. ~ * :ref:`modindex`
.. ~ * :ref:`search`






        



